/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.account;

/**
 * Account
 * @author FantaBlueMystery
 */
public class Account implements IAccount {

	/**
	 * id of account
	 */
	protected int _id;

	/**
	 * username
	 */
	protected String _username;

	/**
	 * password
	 */
	protected String _password;

	/**
	 * getId
	 * @return
	 */
	@Override
	public int getId() {
		return this._id;
	}

	/**
	 * getUsername
	 * @return
	 */
	@Override
	public String getUsername() {
		return this._username;
	}

	/**
	 * getPassword
	 * @return
	 */
	@Override
	public String getPassword() {
		return this._password;
	}
}