/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.account;

import java.util.HashMap;
import java.util.Map;

/**
 * AccountManager
 * @author FantaBlueMystery
 */
public class AccountManager implements IAccountManager {

	/**
	 * account manager
	 */
	static protected IAccountManager _instance;

	/**
	 * getInstance
	 * return current AccountManager instance
	 * @return
	 */
	static public IAccountManager getInstance() {
		if( AccountManager._instance == null ) {
			AccountManager._instance = new AccountManager();
		}

		return AccountManager._instance;
	}

	/**
	 * accounts
	 */
	private HashMap<Integer, IAccount> _accounts = null;

	/**
	 * AccountManager
	 */
	public AccountManager() {
		this._accounts = new HashMap<>();
	}

	/**
	 * getAccount
	 * @param id
	 * @return
	 */
	@Override
	public IAccount getAccount(int id) {
		if( this._accounts == null ) {
			return null;
		}

		return this._accounts.get(id);
	}

	/**
	 * getAccount
	 * @param username
	 * @return
	 */
	@Override
	public IAccount getAccount(String username) {
		if( this._accounts == null ) {
			return null;
		}

		for( Map.Entry<Integer, IAccount> entry : this._accounts.entrySet()) {
			String tusername = entry.getValue().getUsername();

			if( tusername.compareTo(username) == 0 ) {
				return entry.getValue();
			}
		}

		return null;
	}

	/**
	 * existAccount
	 * @param id
	 * @return
	 */
	@Override
	public boolean existAccount(int id) {
		if( this._accounts == null ) {
			return false;
		}

		return this._accounts.containsKey(id);
	}

	/**
	 * existAccount
	 * @param username
	 * @return
	 */
	@Override
	public boolean existAccount(String username) {
		if( this._accounts == null ) {
			return false;
		}

		for( Map.Entry<Integer, IAccount> entry : this._accounts.entrySet()) {
			String tusername = entry.getValue().getUsername();

			if( tusername.compareTo(username) == 0 ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * addAccount
	 * @param account
	 */
	@Override
	public void addAccount(IAccount account) {
		if( this._accounts == null ) {
			return;
		}

		if( !this.existAccount(account.getId()) ) {
			this._accounts.put(account.getId(), account);
		}
	}

	/**
	 * count
	 * @return
	 */
	@Override
	public int count() {
		if( this._accounts == null ) {
			return 0;
		}

		return this._accounts.size();
	}

	/**
	 * comparePassword
	 * @param clientPassword
	 * @param sPwdBuffer
	 * @return
	 */
	@Override
	public boolean comparePassword(String clientPassword, String sPwdBuffer) {
		// password blank check (usw for your version hash check!)
		return (sPwdBuffer.trim().compareTo(clientPassword.trim()) == 0);
	}
}