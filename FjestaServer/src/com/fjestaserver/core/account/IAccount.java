/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.account;

/**
 * IAccount
 * @author FantaBlueMystery
 */
public interface IAccount {

	/**
	 * getId
	 * @return
	 */
	public int getId();

	/**
	 * getUsername
	 * @return
	 */
	public String getUsername();

	/**
	 * getPassword
	 * @return
	 */
	public String getPassword();
}