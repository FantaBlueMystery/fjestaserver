/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.account;

/**
 * IAccountManager
 * @author FantaBlueMystery
 */
public interface IAccountManager {

	/**
	 * getAccount
	 * @param id
	 * @return
	 */
	public IAccount getAccount(int id);

	/**
	 * getAccount
	 * @param username
	 * @return
	 */
	public IAccount getAccount(String username);

	/**
	 * existAccount
	 * @param id
	 * @return
	 */
	public boolean existAccount(int id);

	/**
	 * existAccount
	 * @param username
	 * @return
	 */
	public boolean existAccount(String username);

	/**
	 * addAccount
	 * @param account
	 */
	public void addAccount(IAccount account);

	/**
	 * count
	 * @return
	 */
	public int count();

	/**
	 * comparePassword
	 * @param clientPassword
	 * @param sPwdBuffer
	 * @return
	 */
	public boolean comparePassword(String clientPassword, String sPwdBuffer);
}