/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.characters;

import com.fjestaserver.core.account.IAccount;

/**
 * CharacterExceptionMax
 * @author FantaBlueMystery
 */
public class CharacterExceptionMax extends Exception {

	/**
	 * account
	 */
	protected IAccount _account;

	/**
	 * character
	 */
	protected ICharacterServer _character;

	/**
	 * CharacterExceptionMax
	 * @param account
	 * @param character
	 */
	public CharacterExceptionMax(IAccount account, ICharacterServer character) {
		super("Max character slot reached!");

		this._account = account;
		this._character = character;
	}

	/**
	 * getAccount
	 * @return
	 */
	public IAccount getAccount() {
		return this._account;
	}

	/**
	 * getCharacter
	 * @return
	 */
	public ICharacterServer getCharacter() {
		return this._character;
	}
}