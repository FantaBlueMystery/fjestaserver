/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.characters;

import com.fjestaserver.core.account.IAccount;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * CharacterManager
 * @author FantaBlueMystery
 */
public class CharacterManager implements ICharacterManager {

	// Const
	static public int MAX_CHARACTER = 8;

	/**
	 * character manager
	 */
	static protected ICharacterManager _instance;

	/**
	 * getInstance
	 * return instance of character manager
	 * @return
	 */
	static public ICharacterManager getInstance() {
		if( CharacterManager._instance == null ) {
			CharacterManager._instance = new CharacterManager();
		}

		return CharacterManager._instance;
	}

	/**
	 * list
	 */
	protected HashMap<Long, ICharacterServer> _list = new HashMap<>();

	/**
	 * getSlotLimit
	 * @return
	 */
	@Override
	public int getSlotLimit() {
		return CharacterManager.MAX_CHARACTER;
	}

	/**
	 * getCharacter
	 * @param id
	 * @return
	 */
	@Override
	public ICharacterServer getCharacter(long id) {
		return this._list.get(id);
	}

	/**
	 * getCharacter
	 * @param account
	 * @param slot
	 * @return
	 */
	@Override
	public ICharacterServer getCharacter(IAccount account, int slot) {
		for( Map.Entry<Long, ICharacterServer> entry : this._list.entrySet() ) {
			ICharacterServer charServ = entry.getValue();

			if( (charServ.getAccountId() == account.getId()) && (charServ.getSlot() == slot) ) {
				return charServ;
			}
		}

		return null;
	}

	/**
	 * getCharacters
	 * @param account
	 * @return
	 */
	@Override
	public ArrayList<ICharacterServer> getCharacters(IAccount account) {
		ArrayList<ICharacterServer> characters = new ArrayList<>();

		for( Map.Entry<Long, ICharacterServer> entry : this._list.entrySet() ) {
			ICharacterServer charServ = entry.getValue();

			if( charServ.getAccountId()== account.getId() ) {
				characters.add(charServ);
			}
		}

		return characters;
	}

	/**
	 * countCharacter
	 * @param account
	 * @return
	 */
	@Override
	public int countCharacter(IAccount account) {
		return this.getCharacters(account).size();
	}

	/**
	 * addCharacter
	 * @param account
	 * @param character
	 * @return
	 * @throws CharacterExceptionMax
	 */
	@Override
	public ICharacterServer addCharacter(IAccount account, ICharacterServer character) throws CharacterExceptionMax {
		if( this.countCharacter(account) >= CharacterManager.MAX_CHARACTER ) {
			throw new CharacterExceptionMax(account, character);
		}

		character.setSlot(this.countCharacter(account));
		character.setAccount(account);

		this._list.put(character.getCharacterHandle(), character);

		return character;
	}

	/**
	 * getCharacter
	 * @param charname
	 * @return
	 */
	@Override
	public ICharacterServer getCharacter(String charname) {
		ArrayList<ICharacterServer> characters = new ArrayList<>();

		for( Map.Entry<Long, ICharacterServer> entry : this._list.entrySet() ) {
			ICharacterServer charServ = entry.getValue();

			if( charServ.getCharname().compareTo(charname) == 0 ) {
				return charServ;
			}
		}

		return null;
	}

	/**
	 * existCharacter
	 * @param charname
	 * @return
	 */
	@Override
	public boolean existCharacter(String charname) {
		ICharacterServer character = this.getCharacter(charname);

		return (character != null);
	}

	/**
	 * existCharacter
	 * @param account
	 * @param slot
	 * @return
	 */
	@Override
	public boolean existCharacter(IAccount account, int slot) {
		ICharacterServer character = this.getCharacter(account, slot);

		return (character != null);
	}

	/**
	 * eraseCharacter
	 * @param account
	 * @param slot
	 * @return
	 */
	@Override
	public boolean eraseCharacter(IAccount account, int slot) {
		ICharacterServer eraseChar = null;

		for( Map.Entry<Long, ICharacterServer> entry : this._list.entrySet() ) {
			ICharacterServer charServ = entry.getValue();

			if( charServ.getAccountId()== account.getId() ) {
				if( charServ.getSlot() == slot ) {
					eraseChar = charServ;
					break;
				}
			}
		}

		if( eraseChar != null ) {
			ICharacterServer tmp = this._list.remove(eraseChar.getCharacterHandle());

			if( tmp != null ) {
				return true;
			}
		}

		return false;
	}
}
