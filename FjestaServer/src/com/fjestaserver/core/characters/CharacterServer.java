/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.characters;

import com.fjestacore.common.avatar.AvatarInformation;
import com.fjestacore.common.act.SomeoneEmoticon;
import com.fjestacore.common.act.SomeoneEmoticonStop;
import com.fjestacore.common.act.SomeoneJump;
import com.fjestacore.common.act.SomeoneStop;
import com.fjestacore.common.bat.HpChange;
import com.fjestacore.common.bat.TargetInfo;
import com.fjestacore.common.briefinfo.LoginCharacter;
import com.fjestacore.common.character.CharBase;
import com.fjestacore.common.character.CharMapLogin;
import com.fjestacore.common.character.CharParameterData;
import com.fjestacore.common.character.CharacterEquipment;
import com.fjestacore.common.character.CharacterLook;
import com.fjestacore.common.character.CharacterState;
import com.fjestacore.common.inventory.Inventory;
import com.fjestacore.common.inventory.InventoryType;
import com.fjestacore.common.kq.KQTeamType;
import com.fjestacore.common.kq.KQTeamTypeAck;
import com.fjestacore.common.move.MoveFromTo;
import com.fjestacore.common.move.SomeoneMoveFromTo;
import com.fjestacore.common.position.PositionXY;
import com.fjestacore.common.quest.QuestDoing;
import com.fjestacore.common.quest.QuestDone;
import com.fjestacore.common.quest.QuestRead;
import com.fjestacore.common.quest.QuestRepeat;
import com.fjestacore.common.skill.CharClientSkills;
import com.fjestacore.common.stopemoticon.StopEmoticonDescipt;
import com.fjestacore.packet.act.NC_ACT_SOMEEONEJUMP_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEEMOTICONSTOP_CMD;
import com.fjestacore.packet.bat.NC_BAT_HPCHANGE_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_BASE_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_CHARGEDBUFF_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_CHARTITLE_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_COININFO_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_GAME_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_ITEM_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_PASSIVE_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_QUEST_DOING_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_QUEST_DONE_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_QUEST_READ_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_QUEST_REPEAT_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_SHAPE_CMD;
import com.fjestacore.packet.chars.NC_CHAR_CLIENT_SKILL_CMD;
import com.fjestacore.packet.kq.NC_KQ_TEAM_TYPE_CMD;
import com.fjestacore.packet.map.NC_MAP_LOGIN_ACK;
import com.fjestacore.packet.quest.NC_QUEST_RESET_TIME_ZONE_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEEMOTICON_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEMOVERUN_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEMOVEWALK_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONESTOP_CMD;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.item.IItemDroped;
import com.fjestaserver.core.item.IItemInventory;
import com.fjestaserver.core.item.ItemManager;
import com.fjestaserver.core.map.IMap;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.map.MapObjectType;
import com.fjestaserver.zone.ClientZone;
import com.fjestaserver.zone.ServerZone;
import java.util.HashMap;
import java.util.Map;

/**
 * CharacterServer
 * @author FantaBlueMystery
 */
public class CharacterServer extends CharBase implements ICharacterServer {

	/**
	 * account id, owner of this char
	 */
	protected int _accountId = 0;

	/**
	 *
	 */
	protected ClientZone _clientZone = null;

	/**
	 * map
	 */
	protected IMap _map = null;

	/**
	 * character look
	 */
	protected CharacterLook _look = new CharacterLook();

	/**
	 * parameter
	 */
	protected CharParameterData _parameter = new CharParameterData();

	/**
	 * inventories
	 */
	protected Map<InventoryType, IItemInventory> _inventories = new HashMap<>();

	/**
	 * emoticon
	 */
	protected StopEmoticonDescipt _emoticon = new StopEmoticonDescipt();

	/**
	 * guild id
	 */
	protected long _guildId = 0;

	/**
	 * move speed
	 */
	protected int _moveSpeed = 100;

	/**
	 * CharacterServer
	 */
	public CharacterServer() {
		InventoryType[] invTypes = new InventoryType[]{
			InventoryType.CHAR_INVENTORY,
			InventoryType.EQUIPPED,
			InventoryType.MINIHOUSE_SKIN,
			InventoryType.ACTION_BOX,
			InventoryType.REWARD_STORAGE,
			};

		for( InventoryType invType: invTypes ) {
			IItemInventory ii = ItemManager.getInstance().getInventory(this._characterHandle, invType);

			if( ii == null ) {
				ii = ItemManager.getInstance().createInventory(_characterHandle, invType);
			}

			this._inventories.put(invType, ii);
		}

		// set Default

		this._bars.setHp(100);
		this._bars.setSp(100);

		this._parameter.getBarsMax().setHp(100);
		this._parameter.getBarsMax().setSp(100);
	}

	/**
	 * getAccountId
	 * @return
	 */
	@Override
	public int getAccountId() {
		return this._accountId;
	}

	/**
	 * setAccountId
	 * @param id
	 */
	@Override
	public void setAccountId(int id) {
		this._accountId = id;
	}

	/**
	 * setAccount
	 * @param account
	 */
	@Override
	public void setAccount(IAccount account) {
		this._accountId = account.getId();
	}

	/**
	 * getClientZone
	 * @return
	 */
	@Override
	public ClientZone getClientZone() {
		return this._clientZone;
	}

	/**
	 * setClientZone
	 * @param cz
	 */
	@Override
	public void setClientZone(ClientZone cz) {
		this._clientZone = cz;
	}

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return (int) this._characterHandle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._characterHandle = handle;
	}

	/**
	 * setMap
	 * @param map
	 */
	@Override
	public void setMap(IMap map) {
		this._map = map;

		String mapname = "";

		if( map == null ) {
			mapname = map.getMapInfo().getMapName();
		}

		this._mapname.setContent(mapname);
	}

	/**
	 * getMap
	 * @return
	 */
	@Override
	public IMap getMap() {
		return this._map;
	}

	/**
	 * getInventory
	 * @param type
	 * @return
	 */
	@Override
	public IItemInventory getInventory(InventoryType type) {
		return this._inventories.get(type);
	}

	/**
	 * setLook
	 * @param look
	 */
	@Override
	public void setLook(CharacterLook look) {
		this._look = look;
	}

	/**
	 * getLook
	 * @return
	 */
	@Override
	public CharacterLook getLook() {
		return this._look;
	}

	/**
	 * getParameter
	 * @return
	 */
	@Override
	public CharParameterData getParameter() {
		return this._parameter;
	}

	/**
	 * getEmoticon
	 * @return
	 */
	@Override
	public StopEmoticonDescipt getEmoticon() {
		return this._emoticon;
	}

	/**
	 * getAvatar
	 * @return
	 */
	@Override
	public AvatarInformation getAvatar() {
		AvatarInformation av = new AvatarInformation();

		av.setCharacterHandle(this.getCharacterHandle());
		av.setCharname(this.getCharname());
		av.setLevel(this.getLevel());
		av.setSlot(this.getSlot());
		av.setMapname(this.getMapname());
		av.setLook(this.getLook());

		// TODO

		return av;
	}

	/**
	 * getEquipment
	 * @return
	 */
	public CharacterEquipment getEquipment() {
		return new CharacterEquipment(
			(Inventory) this.getInventory(InventoryType.EQUIPPED));
	}

	/**
	 * getLoginCharacter
	 * @return
	 */
	@Override
	public LoginCharacter getLoginCharacter() {
		LoginCharacter lc = new LoginCharacter();

		lc.setObjectHandle(this.getObjectHandle());
		lc.setName(this.getCharname());
		lc.setPosition(this.getPosition());
		lc.setState(CharacterState.PLAYER);
		lc.setCharClass(this.getLook().getPlayerClass());
		lc.setLook(this.getLook());
		lc.setEquipment(this.getEquipment());
		lc.setLevel(this.getLevel());
		lc.setEmoticon(this._emoticon);
		lc.setGuildId((int) this._guildId);

		return lc;
	}

	/**
	 * getType
	 * @return
	 */
	@Override
	public MapObjectType getType() {
		return MapObjectType.PLAYER;
	}

	/**
	 * getTargetInfo
	 * @return
	 */
	@Override
	public TargetInfo getTargetInfo() {
		TargetInfo ti = new TargetInfo();

		ti.setFlag(0x80);
		ti.setObjectHandle(this.getObjectHandle());
		ti.setTargetHp(this._bars.getHp());
		ti.setTargetSp(this._bars.getSp());
		ti.setTargetLp(this._bars.getLp());
		ti.setTargetMaxHp(this._parameter.getBarsMax().getHp());
		ti.setTargetMaxSp(this._parameter.getBarsMax().getSp());
		ti.setTargetMaxLp(this._parameter.getBarsMax().getLp());
		ti.setTargetLevel(this.getLevel());

		return ti;
	}

	/**
	 * getGuildId
	 * @return
	 */
	@Override
	public long getGuildId() {
		return this._guildId;
	}

	/**
	 * setGuildId
	 * @param id
	 */
	@Override
	public void setGuildId(long id) {
		this._guildId = id;
	}

	/**
	 * mapLogin
	 */
	@Override
	public void mapLogin() {
		if( this._clientZone == null ) {
			return;
		}

		this._clientZone.writePacket(new NC_CHAR_CLIENT_BASE_CMD((CharBase) this));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_SHAPE_CMD(this.getLook()));

		// TODO by char
		this._clientZone.writePacket(new NC_CHAR_CLIENT_QUEST_DOING_CMD(new QuestDoing(this)));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_QUEST_DONE_CMD(new QuestDone(this)));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_QUEST_READ_CMD(new QuestRead(this)));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_QUEST_REPEAT_CMD(new QuestRepeat(this)));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_SKILL_CMD(new CharClientSkills(this)));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_PASSIVE_CMD());

		this._clientZone.writePacket(new NC_CHAR_CLIENT_ITEM_CMD((Inventory) this.getInventory(InventoryType.CHAR_INVENTORY)));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_ITEM_CMD((Inventory) this.getInventory(InventoryType.EQUIPPED)));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_ITEM_CMD((Inventory) this.getInventory(InventoryType.MINIHOUSE_SKIN)));
		this._clientZone.writePacket(new NC_CHAR_CLIENT_ITEM_CMD((Inventory) this.getInventory(InventoryType.ACTION_BOX)));

		this._clientZone.writePacket(new NC_CHAR_CLIENT_CHARTITLE_CMD());
		this._clientZone.writePacket(new NC_CHAR_CLIENT_CHARGEDBUFF_CMD());

		this._clientZone.writePacket(new NC_CHAR_CLIENT_GAME_CMD());
		this._clientZone.writePacket(new NC_CHAR_CLIENT_COININFO_CMD());
		this._clientZone.writePacket(new NC_QUEST_RESET_TIME_ZONE_CMD());

		this._clientZone.writePacket(new NC_MAP_LOGIN_ACK(new CharMapLogin(
			this.getObjectHandle(),
			this.getParameter(),
			this.getPosition().getPositionXY()
			)));
	}

	/**
	 * mapLoginComplete
	 */
	@Override
	public void mapLoginComplete() {
		if( this._clientZone == null ) {
			return;
		}

		this._clientZone.writePacket(new NC_KQ_TEAM_TYPE_CMD(new KQTeamTypeAck(KQTeamType.KQTT_MAX)));

		// ---------------------------------------------------------------------

		long hp = this._bars.getHp();

		if( hp == 0 ) {
			hp = this._parameter.getBarsMax().getHp() / 2;
		}

		// todo
		this._clientZone.writePacket(new NC_BAT_HPCHANGE_CMD(new HpChange(hp, 0x0d3b)));
	}

	/**
	 * useEmote
	 * @param id
	 */
	@Override
	public void useEmote(int id) {
		this._emoticon.setEmoticonId(id);

		if( this._clientZone == null ) {
			return;
		}

		ServerZone sz = this._clientZone.getServer();

		sz.writePacketInRange(
			new NC_ACT_SOMEONEEMOTICON_CMD(
				new SomeoneEmoticon(this.getObjectHandle(), this.getEmoticon().getEmoticonId())
			),
			this.getPosition()
			);
	}

	/**
	 * useJump
	 * @param byClient
	 */
	@Override
	public void useJump(boolean byClient) {
		if( this._clientZone == null ) {
			return;
		}

		ServerZone sz = this._clientZone.getServer();

		sz.writePacketInRange(
			new NC_ACT_SOMEEONEJUMP_CMD(
				new SomeoneJump(this.getObjectHandle())
			),
			this.getPosition(),
			( byClient ? this._clientZone.getSession().getId() : null)
			);
	}

	/**
	 * stopEmote
	 */
	@Override
	public void stopEmote() {
		this._emoticon.setEmoticonId(0);

		if( this._clientZone == null ) {
			return;
		}

		ServerZone sz = this._clientZone.getServer();

		sz.writePacketInRange(
			new NC_ACT_SOMEONEEMOTICONSTOP_CMD(
				new SomeoneEmoticonStop(this.getObjectHandle(), this.getEmoticon())
			),
			this.getPosition()
			);
	}

	/**
	 * moveRun
	 * @param move
	 */
	@Override
	public void moveRun(MoveFromTo move) {
		this.getPosition().setPosition(move.getTo());

		if( this._clientZone == null ) {
			return;
		}

		ServerZone sz = this._clientZone.getServer();

		sz.writePacketInRange(new NC_ACT_SOMEONEMOVERUN_CMD(
			new SomeoneMoveFromTo(this.getObjectHandle(), move, this._moveSpeed)),
			this.getPosition(),
			this._clientZone.getSession().getId()
			);
	}

	/**
	 * moveWalk
	 * @param move
	 */
	@Override
	public void moveWalk(MoveFromTo move) {
		this.getPosition().setPosition(move.getTo());

		if( this._clientZone == null ) {
			return;
		}

		ServerZone sz = this._clientZone.getServer();

		sz.writePacketInRange(new NC_ACT_SOMEONEMOVEWALK_CMD(
			new SomeoneMoveFromTo(this.getObjectHandle(), move, this._moveSpeed)),
			this.getPosition(),
			this._clientZone.getSession().getId()
			);
	}

	/**
	 * moveStop
	 * @param pos
	 */
	@Override
	public void moveStop(PositionXY pos) {
		this.getPosition().setPosition(pos);

		if( this._clientZone == null ) {
			return;
		}

		ServerZone sz = this._clientZone.getServer();

		sz.writePacketInRange(
			new NC_ACT_SOMEONESTOP_CMD(new SomeoneStop(
				this.getObjectHandle(),
				pos
				)),
			this.getPosition(),
			this._clientZone.getSession().getId()
			);
	}

	/**
	 * useItem
	 * @param slot
	 * @param lot
	 */
	@Override
	public void useItem(int slot, int lot) {

	}

	/**
	 * pickItem
	 * @param itemHandle
	 */
	@Override
	public void pickItem(int itemHandle) {
		if( this._map == null ) {
			return;
		}

		IItemDroped itemdrop = (IItemDroped) this._map.getObjectList().getItem(itemHandle);

		if( itemdrop == null ) {
			return;
		}

		IItemInventory ii = this._inventories.get(InventoryType.CHAR_INVENTORY);

		if( ii.getItemSizeMax() > ii.getItemCount() ) {
			

			ii.addItem();
		}
	}
}