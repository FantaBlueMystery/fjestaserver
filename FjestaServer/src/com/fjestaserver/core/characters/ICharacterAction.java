/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.characters;

import com.fjestaserver.core.map.IMapLivingObject;

/**
 * ICharacterAction
 * @author FantaBlueMystery
 */
public interface ICharacterAction extends IMapLivingObject {

	/**
	 * useEmote
	 * @param id
	 */
	public void useEmote(int id);

	/**
	 * stopEmote
	 */
	public void stopEmote();

	/**
	 * useJump
	 * @param byClient
	 */
	public void useJump(boolean byClient);

	/**
	 * useItem
	 * @param slot
	 * @param lot
	 */
	public void useItem(int slot, int lot);

	/**
	 * pickItem
	 * @param itemHandle
	 */
	public void pickItem(int itemHandle);
}