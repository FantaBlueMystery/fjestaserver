/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.characters;

import com.fjestaserver.core.account.IAccount;
import java.util.ArrayList;

/**
 * ICharacterManager
 * @author FantaBlueMystery
 */
public interface ICharacterManager {

	/**
	 * getSlotLimit
	 * @return
	 */
	public int getSlotLimit();

	/**
	 * getCharacter
	 * @param id
	 * @return
	 */
	public ICharacterServer getCharacter(long id);

	/**
	 * getCharacter
	 * @param account
	 * @param slot
	 * @return
	 */
	public ICharacterServer getCharacter(IAccount account, int slot);

	/**
	 * getCharacter
	 * @param charname
	 * @return
	 */
	public ICharacterServer getCharacter(String charname);

	/**
	 * getCharacters
	 * @param account
	 * @return
	 */
	public ArrayList<ICharacterServer> getCharacters(IAccount account);

	/**
	 * countCharacter
	 * @param account
	 * @return
	 */
	public int countCharacter(IAccount account);

	/**
	 * addCharacter
	 * @param account
	 * @param character
	 * @return
	 * @throws com.fjestaserver.core.characters.CharacterExceptionMax
	 */
	public ICharacterServer addCharacter(IAccount account, ICharacterServer character) throws CharacterExceptionMax;

	/**
	 * existCharacter
	 * @param charname
	 * @return
	 */
	public boolean existCharacter(String charname);

	/**
	 * existCharacter
	 * @param account
	 * @param slot
	 * @return
	 */
	public boolean existCharacter(IAccount account, int slot);

	/**
	 * eraseCharacter
	 * @param account
	 * @param slot
	 * @return
	 */
	public boolean eraseCharacter(IAccount account, int slot);
}