/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.characters;

import com.fjestacore.common.avatar.AvatarInformation;
import com.fjestacore.common.briefinfo.LoginCharacter;
import com.fjestacore.common.character.CharParameterData;
import com.fjestacore.common.character.CharacterLook;
import com.fjestacore.common.inventory.InventoryType;
import com.fjestacore.common.position.PositionXYR;
import com.fjestacore.common.stopemoticon.StopEmoticonDescipt;
import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.item.IItemInventory;
import com.fjestaserver.zone.ClientZone;

/**
 * ICharacter
 * @author FantaBlueMystery
 */
public interface ICharacterServer extends ICharacterHandle, IObjectHandle, ICharacterAction {

	/**
	 * getClientZone
	 * @return
	 */
	public ClientZone getClientZone();

	/**
	 * setClientZone
	 * @param cz
	 */
	public void setClientZone(ClientZone cz);

	/**
	 * getAccountId
	 * @return
	 */
	public int getAccountId();

	/**
	 * setAccountId
	 * @param accountid
	 */
	public void setAccountId(int accountid);

	/**
	 * setAccount
	 * @param account
	 */
	public void setAccount(IAccount account);

	/**
	 * getSlot
	 * @return
	 */
	public int getSlot();

	/**
	 * setSlot
	 * @param slot
	 */
	public void setSlot(int slot);

	/**
	 * getCharname
	 * @return
	 */
	public String getCharname();

	/**
	 * setLook
	 * @param look
	 */
	public void setLook(CharacterLook look);

	/**
	 * getLook
	 * @return
	 */
	public CharacterLook getLook();

	/**
	 * getInventory
	 * @param type
	 * @return
	 */
	public IItemInventory getInventory(InventoryType type);

	/**
	 * getPosition
	 * @return
	 */
	public PositionXYR getPosition();

	/**
	 * getAvatar
	 * @return
	 */
	public AvatarInformation getAvatar();

	/**
	 * getParameter
	 * @return
	 */
	public CharParameterData getParameter();

	/**
	 * getLoginCharacter
	 * @return
	 */
	public LoginCharacter getLoginCharacter();

	/**
	 * getAdminLevel
	 * @return
	 */
	public int getAdminLevel();

	/**
	 * setAdminLevel
	 * @param level
	 */
	public void setAdminLevel(int level);

	/**
	 *
	 * @return
	 */
	public StopEmoticonDescipt getEmoticon();

	/**
	 * getGuildId
	 * @return
	 */
	public long getGuildId();

	/**
	 * setGuildId
	 * @param id
	 */
	public void setGuildId(long id);

	/**
	 * mapLogin
	 */
	public void mapLogin();

	/**
	 * mapLoginComplete
	 */
	public void mapLoginComplete();
}