/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command;

import com.fjestacore.server.ISession;
import com.fjestaserver.core.command.admin.AdminLevel;
import com.fjestaserver.core.command.admin.Coord;
import com.fjestaserver.core.command.admin.DropItem;
import com.fjestaserver.core.command.admin.SetAdminLevel;
import com.fjestaserver.core.command.admin.UseEmote;
import com.fjestaserver.core.command.admin.UseJump;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.core.word.WordFilterManager;
import java.util.HashMap;
import java.util.Map;

/**
 * CommandManager
 * @author FantaBlueMystery
 */
public class CommandManager {

	/**
	 * cmd begin char
	 */
	public static final String CMD_CHAR = "&";

	/**
	 * instance of command manager
	 */
	static protected CommandManager _instance = null;

	/**
	 * getInstance
	 * @return
	 */
	static public CommandManager getInstance() {
		if( CommandManager._instance == null ) {
			CommandManager._instance = new CommandManager();
		}

		return CommandManager._instance;
	}

	/**
	 * commands
	 */
	protected Map<String, ICommand> _commands = new HashMap<>();

	/**
	 * CommandManager
	 */
	public CommandManager() {
		this.add(new AdminLevel());
		this.add(new Coord());
		this.add(new SetAdminLevel());
		this.add(new DropItem());
		this.add(new UseEmote());
		this.add(new UseJump());
	}

	/**
	 * add
	 * @param cmd
	 */
	public void add(ICommand cmd) {
		this._commands.put(cmd.getName(), cmd);
	}

	/**
	 * process
	 * @param content
	 * @param client
	 * @return
	 * @throws CommandNotFoundException
	 */
	public String process(String content, Client client) throws CommandNotFoundException {
		ISession tsession = client.getSession();

		if( tsession == null ) {
			return null;
		}

		if( content.startsWith(CommandManager.CMD_CHAR) ) {
			String[] cmdparts = content.split(" ", 2);

			if( cmdparts.length >= 1 ) {
				String cmdname = cmdparts[0].substring(1);

				ICommand cmd = this._commands.get(cmdname);

				if( cmd != null ) {
					String[] args = new String[]{};

					if( cmdparts.length == 2 ) {
						args = cmdparts[1].split(" ");
					}

					if( client.getSession().getCharacter().getAdminLevel() >= cmd.getAdminLevel() ) {
						cmd.onPerformed(client, args);
					}
				}
				else {
					throw new CommandNotFoundException(content, client);
				}
			}

			return null;
		}

		WordFilterManager wfm = WordFilterManager.getInstance();

		if( wfm.isFoundBadWord(content) ) {
			content = wfm.replaceBadWords(content);
		}

		return content;
	}
}