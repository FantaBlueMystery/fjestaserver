/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command;

import com.fjestaserver.core.net.Client;

/**
 * CommandNotFoundException
 * @author FantaBlueMystery
 */
public class CommandNotFoundException extends Exception {

	/**
	 * server client
	 */
	protected Client _client = null;

	/**
	 * CommandNotFoundException
	 * @param content
	 */
	public CommandNotFoundException(String content) {
		super(content);
	}

	/**
	 * CommandNotFoundException
	 * @param content
	 * @param client
	 */
	public CommandNotFoundException(String content, Client client) {
		super(content);

		this._client = client;
	}

	/**
	 * getClient
	 * @return
	 */
	public Client getClient() {
		return this._client;
	}
}