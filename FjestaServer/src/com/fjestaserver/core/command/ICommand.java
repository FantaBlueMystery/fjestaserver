/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery, SeerOfVoid420
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command;

import com.fjestaserver.core.net.Client;

/**
 * ICommand
 * @author FantaBlueMystery
 */
public interface ICommand {

	/**
     * getAdminLevel
     * @return Minimum level to run command
     */
    public default int getAdminLevel() {
        return 100;
    }

	/**
	 * getName
	 * @return
	 */
	public String getName();

	/**
	 * onPerformed
	 * @param client
	 * @param args
	 */
	public void onPerformed(Client client, String[] args);
}