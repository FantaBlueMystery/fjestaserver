/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command.admin;

import com.fjestacore.common.character.CharAdminLevel;
import com.fjestacore.packet.chars.NC_CHAR_ADMIN_LEVEL_INFORM_CMD;
import com.fjestaserver.core.command.ICommand;
import com.fjestaserver.core.net.Client;

/**
 * AdminLevel
 * @author FantaBlueMystery
 */
public class AdminLevel implements ICommand {

	/**
	 * getCmdName
	 * @return
	 */
	@Override
	public String getName() {
		return "adminlevel";
	}

	/**
	 * onPerformed
	 * @param client
	 * @param args
	 */
	@Override
	public void onPerformed(Client client, String[] args) {
		int al = client.getSession().getCharacter().getAdminLevel();

		if( al > 0 ) {
			client.writePacket(new NC_CHAR_ADMIN_LEVEL_INFORM_CMD(new CharAdminLevel(al)));
		}
	}
}
