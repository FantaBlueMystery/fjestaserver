/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command.admin;

import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.command.ICommand;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.zone.ClientZone;

/**
 * Coord
 * @author FantaBlueMystery
 */
public class Coord implements ICommand {

	/**
	 * getCmdName
	 * @return
	 */
	@Override
	public String getName() {
		return "coord";
	}

	/**
	 * onPerformed
	 * @param client
	 * @param args
	 */
	@Override
	public void onPerformed(Client client, String[] args) {
		ISession tsession = client.getSession();
		ICharacterServer cs = tsession.getCharacter();

		((ClientZone) client).sendChat(
			"Your coord:" +
			" x: " + Long.toString(cs.getPosition().getX()) +
			" y: " + Long.toString(cs.getPosition().getY())
			);
	}
}