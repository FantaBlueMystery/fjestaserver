/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command.admin;

import com.fjestaserver.core.command.ICommand;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.core.session.ISession;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Debug
 * @author FantaBlueMystery
 */
public class Debug implements ICommand {

	/**
	 * getName
	 * @return
	 */
	@Override
	public String getName() {
		return "debug";
	}

	/**
	 * onPerformed
	 * @param client
	 * @param args
	 */
	@Override
	public void onPerformed(Client client, String[] args) {
		ISession tsession = client.getSession();

		if( args.length >= 1 ) {
			switch( args[0] ) {
				case "1":
					client.setDebugging(true);

					try {
						FileHandler fh = new FileHandler(tsession.getId() + "-" + tsession.getAccount().getUsername() + ".log");

						client.getLogger().addHandler(fh);
						fh.setFormatter(new SimpleFormatter());
					}
					catch( IOException ex ) {
						Logger.getLogger(Debug.class.getName()).log(Level.SEVERE, null, ex);
					}

					break;

				case "0":
					client.setDebugging(false);

					for( Handler th: client.getLogger().getHandlers() ) {
						client.getLogger().removeHandler(th);
					}
					break;
			}
		}
	}
}
