/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command.admin;

import com.fjestacore.common.briefinfo.DropedItemState;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.command.ICommand;
import com.fjestaserver.core.item.ItemDrop;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.zone.ServerZone;

/**
 * DropItem
 * @author FantaBlueMystery
 */
public class DropItem implements ICommand {

	/**
	 * getName
	 * @return
	 */
	@Override
	public String getName() {
		return "dropitem";
	}

	/**
	 * onPerformed
	 * @param client
	 * @param args
	 */
	@Override
	public void onPerformed(Client client, String[] args) {
		if( args.length >= 1 ) {
			ServerZone sz = (ServerZone) client.getServer();

			ICharacterServer cs = client.getSession().getCharacter();

			sz.dropItem(new ItemDrop(
				(IMapObject) cs,
				Integer.valueOf(args[0]),
				DropedItemState.CANLOOT
				));
		}
	}
}