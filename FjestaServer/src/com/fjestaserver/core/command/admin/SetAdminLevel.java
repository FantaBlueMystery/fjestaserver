/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command.admin;

import com.fjestacore.common.ColorID;
import com.fjestacore.common.character.CharAdminLevel;
import com.fjestacore.packet.chars.NC_CHAR_ADMIN_LEVEL_INFORM_CMD;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.command.ICommand;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.zone.ClientZone;

/**
 * SetAdminLevel
 * @author FantaBlueMystery
 */
public class SetAdminLevel implements ICommand {

	/**
	 * getCmdName
	 * @return
	 */
	@Override
	public String getName() {
		return "setadminlevel";
	}

	/**
	 * onPerformed
	 * @param client
	 * @param args
	 */
	@Override
	public void onPerformed(Client client, String[] args) {
		try {
			if( args.length > 0 ) {
				IMapObject mo = client.getSession().getSelectedTarget();
				ICharacterServer cs = client.getSession().getCharacter();

				if( mo instanceof ICharacterServer ) {
					cs = (ICharacterServer) mo;
				}

				cs.setAdminLevel(Integer.valueOf(args[0]));

				int al = client.getSession().getCharacter().getAdminLevel();

				client.writePacket(new NC_CHAR_ADMIN_LEVEL_INFORM_CMD(new CharAdminLevel(al)));

				// -------------------------------------------------------------

				((ClientZone) client).sendChat(
					"SetAdminLevel: " + Integer.toString(al) +  " for User: " + cs.getCharname(),
					ColorID.CHAT_GMSHOUT
					);
			}
		}
		catch( Exception ex ) {
		}
	}
}