/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command.admin;

import com.fjestaserver.core.characters.ICharacterAction;
import com.fjestaserver.core.command.ICommand;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.zone.ClientZone;

/**
 * UseEmote
 * @author FantaBlueMystery
 */
public class UseEmote implements ICommand {

	/**
	 * getName
	 * @return
	 */
	@Override
	public String getName() {
		return "useemote";
	}

	/**
	 * onPerformed
	 * @param client
	 * @param args
	 */
	@Override
	public void onPerformed(Client client, String[] args) {
		if( args.length > 0 ) {
			IMapObject mo = client.getSession().getSelectedTarget();

			if( mo instanceof ICharacterAction ) {
				int emoteId = Integer.valueOf(args[0]);
				ICharacterAction ca = (ICharacterAction) mo;

				ca.useEmote(emoteId);

				((ClientZone) client).sendNotice("Character Emote: " + args[0]);
			}
			else {
				((ClientZone) client).sendNotice("Please select a character!");
			}
		}
		else {
			((ClientZone) client).sendNotice("Command arg to short!");
		}
	}
}