/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.command.admin;

import com.fjestaserver.core.characters.ICharacterAction;
import com.fjestaserver.core.command.ICommand;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.zone.ClientZone;

/**
 * UseJump
 * @author FantaBlueMystery
 */
public class UseJump implements ICommand {

	/**
	 * getName
	 * @return
	 */
	@Override
	public String getName() {
		return "usejump";
	}

	/**
	 * onPerformed
	 * @param client
	 * @param args
	 */
	@Override
	public void onPerformed(Client client, String[] args) {
		IMapObject mo = client.getSession().getSelectedTarget();

		if( mo instanceof ICharacterAction ) {
			ICharacterAction ca = (ICharacterAction) mo;

			ca.useJump(false);

			((ClientZone) client).sendNotice("Character Jump.");
		}
		else {
			((ClientZone) client).sendNotice("Please select a character!");
		}
	}
}