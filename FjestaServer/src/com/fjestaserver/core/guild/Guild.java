/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.guild;

/**
 * Guild
 * @author FantaBlueMystery
 */
public class Guild implements IGuild {

	/**
	 * GM Guild id
	 */
	static public long DEV_GUILD_ID		= 767;
	static public long GM_GUILD_ID		= 768;
	static public long DEFAULT_START_ID = 1000;

	/**
	 * id
	 */
	protected long _id = 0;

	/**
	 * name
	 */
	protected String _name = "";

	/**
	 * Guild
	 */
	public Guild() {}

	/**
	 * Guild
	 * @param id
	 * @param name
	 */
	public Guild(long id, String name) {
		this._id	= id;
		this._name	= name;
	}

	/**
	 * getId
	 * @return
	 */
	@Override
	public long getId() {
		return this._id;
	}

	/**
	 * getName
	 * @return
	 */
	@Override
	public String getName() {
		return this._name;
	}

	/**
	 * setName
	 * @param name
	 */
	public void setName(String name) {
		this._name = name;
	}
}