/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.guild;

import java.util.HashMap;

/**
 * GuildManager
 * @author FantaBlueMystery
 */
public class GuildManager implements IGuildManager {

	/**
	 * guild manager
	 */
	static protected IGuildManager _instance;

	/**
	 * getInstance
	 * return current GuildManager instance
	 * @return
	 */
	static public IGuildManager getInstance() {
		if( GuildManager._instance == null ) {
			GuildManager._instance = new GuildManager();
		}

		return GuildManager._instance;
	}

	/**
	 * guilds
	 */
	private HashMap<Long, IGuild> _guildlist = null;

	/**
	 * GuildManager
	 */
	public GuildManager() {
		this._guildlist = new HashMap<>();
	}

	/**
	 * getGuild
	 * @param id
	 * @return
	 */
	@Override
	public IGuild getGuild(long id) {
		return this._guildlist.get(id);
	}

	/**
	 * addGuild
	 * @param guild
	 */
	@Override
	public void addGuild(IGuild guild) {
		this._guildlist.put(guild.getId(), guild);
	}
}