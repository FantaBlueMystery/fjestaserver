/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.guild;

/**
 * IGuild
 * @author FantaBlueMystery
 */
public interface IGuild {

	/**
	 * getId
	 * @return
	 */
	public long getId();

	/**
	 * getName
	 * @return
	 */
	public String getName();
}