/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.guild;

/**
 * IGuildManager
 * @author FantaBlueMystery
 */
public interface IGuildManager {

	/**
	 * getGuild
	 * @param id
	 * @return
	 */
	public IGuild getGuild(long id);

	/**
	 * addGuild
	 * @param guild
	 */
	public void addGuild(IGuild guild);
}