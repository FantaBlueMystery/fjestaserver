/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.handle;

import java.util.Stack;

/**
 * HandleIdManager
 * @author FantaBlueMystery
 */
public class HandleIdManager {

	/**
	 * free index size
	 */
	protected int _freeIndexSize = 1000;

	/**
	 * max index
	 */
	protected long _maxIndex = 0;

	/**
	 * cur index
	 */
	protected long _curIndex = 0;

	/**
	 * free Index
	 */
	protected Stack<Long> _freeIndex = new Stack<>();

	/**
	 * HandleManager
	 */
	public HandleIdManager() {
		this._maxIndex = 0xffffffffL;
		this._updateStack();
	}

	/**
	 * HandlerManager
	 * @param maxIndex
	 */
	public HandleIdManager(long maxIndex) {
		this._maxIndex = maxIndex;
		this._updateStack();
	}

	/**
	 * _updateStack
	 */
	private void _updateStack() {
		if( this._freeIndex.size() <  this._freeIndexSize ) {
			long nextIndex = this._curIndex + this._freeIndexSize;

			while( (this._curIndex <= this._maxIndex) && (this._curIndex <= nextIndex) ) {
				this._curIndex++;

				this._freeIndex.push(this._curIndex);
			}
		}
	}

	/**
	 * getNewHandleId
	 * @return
	 */
	public Long getNewHandleId() {
		if( this._freeIndex.empty() ) {
			this._updateStack();
		}

		return this._freeIndex.pop();
	}

	/**
	 * freeHandle
	 * @param handleId
	 */
	public void freeHandle(long handleId) {
		int index = this._freeIndex.search(handleId);

		if( index == -1 ) {
			this._freeIndex.add(handleId);
		}
	}
}
