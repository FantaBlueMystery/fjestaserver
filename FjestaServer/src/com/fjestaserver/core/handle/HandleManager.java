/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.handle;

/**
 * HandleManager
 * @author FantaBlueMystery
 */
public class HandleManager implements IHandleManager {

	/**
	 * handler manager
	 */
	static protected IHandleManager _instance;

	/**
	 * getInstance
	 * return current HandleManager instance
	 * @return
	 */
	static public IHandleManager getInstance() {
		if( HandleManager._instance == null ) {
			HandleManager._instance = new HandleManager();
		}

		return HandleManager._instance;
	}

	/**
	 * world handler
	 */
	protected HandleIdManager _worldHandler;

	/**
	 * object handler
	 */
	protected HandleIdManager _objectHandler;

	/**
	 * HandlerManager
	 */
	public HandleManager() {
		this._worldHandler = new HandleIdManager();
		this._objectHandler = new HandleIdManager();
	}

	/**
	 * getFreeWorldHandle
	 * @return
	 */
	@Override
	public int getFreeWorldHandle() {
		long handle = this._worldHandler.getNewHandleId();

		return ((int) handle);
	}

	/**
	 * resetWorldHandle
	 * @param handle
	 */
	@Override
	public void resetWorldHandle(int handle) {
		this._worldHandler.freeHandle(handle);
	}

	/**
	 * getFreeObjectHandle
	 * @return
	 */
	@Override
	public int getFreeObjectHandle() {
		long handle = this._objectHandler.getNewHandleId();

		return ((int) handle);
	}

	/**
	 * resetObjectHandle
	 * @param handle
	 */
	@Override
	public void resetObjectHandle(int handle) {
		this._objectHandler.freeHandle(handle);
	}
}