/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.handle;

/**
 * IHandleManager
 * @author FantaBlueMystery
 */
public interface IHandleManager {

	/**
	 * getFreeWorldHandle
	 * @return
	 */
	public int getFreeWorldHandle();

	/**
	 * resetServerHandle
	 * @param handle
	 */
	public void resetWorldHandle(int handle);

	// -------------------------------------------------------------------------

	/**
	 * getFreeObjectHandle
	 * @return
	 */
	public int getFreeObjectHandle();

	/**
	 * resetObjectHandle
	 * @param handle
	 */
	public void resetObjectHandle(int handle);
}