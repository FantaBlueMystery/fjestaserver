/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.heartbeat;

import com.fjestacore.packet.misc.NC_MISC_HEARTBEAT_REQ;
import com.fjestaserver.core.net.Client;
import java.sql.Timestamp;
import java.util.logging.Level;

/**
 * Heartbeat
 * @author FantaBlueMystery
 */
public class Heartbeat {

	/**
	 * next time beat
	 */
	protected long _nextTimeBeat = 0;

	/**
	 * beat seac
	 */
	protected int _beatSec = 30;

	/**
	 * max open beat
	 */
	protected int _maxOpenBeat = 20;

	/**
	 * open beat
	 */
	protected int _openBeat = 0;

	/**
	 * Heartbeat
	 */
	public Heartbeat() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		this._nextTimeBeat	= timestamp.getTime();
	}

	/**
	 * performe
	 * @param client
	 */
	public void performe(Client client) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		long ctime = timestamp.getTime();

		if( this._openBeat > this._maxOpenBeat ) {
			client.log(Level.WARNING, "Heatbeat timeout, disconnect client.");
			client.getServer().removeClient(client);
		}
		else {
			if( ctime > this._nextTimeBeat ) {
				NC_MISC_HEARTBEAT_REQ tping = new NC_MISC_HEARTBEAT_REQ();

				if( client.writePacket(tping) ) {
					this._nextTimeBeat = ctime + (1000 * this._beatSec);
					this._openBeat++;
				}
			}
		}
	}

	/**
	 * onBeat
	 */
	public void onBeat() {
		this._openBeat = 0;
	}
}