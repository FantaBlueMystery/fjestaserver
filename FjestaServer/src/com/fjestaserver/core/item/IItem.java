/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.item;

/**
 * IItem
 * @author FantaBlueMystery
 */
public interface IItem {

	/**
	 * getUId
	 * return the Unique id
	 * @return
	 */
	public long getUId();

	/**
	 * getId
	 * return item id
	 * @return
	 */
	public int getId();
}
