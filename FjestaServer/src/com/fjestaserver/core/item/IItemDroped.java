/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.item;

/**
 * IItemDroped
 * @author FantaBlueMystery
 */
public interface IItemDroped {

	/**
	 * getItem
	 * @return
	 */
	public IItem getItem();

	/**
	 * setItem
	 * @param item
	 */
	public void setItem(IItem item);
}