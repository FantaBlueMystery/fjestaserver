/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.item;

import com.fjestacore.common.inventory.InventoryType;

/**
 * IItemInventory
 * @author FantaBlueMystery
 */
public interface IItemInventory {

	/**
	 * getCharacterHandle
	 * character owner of inventory
	 * @return
	 */
	public long getCharacterHandle();

	/**
	 * getType
	 * @return
	 */
	public InventoryType getType();

	/**
	 * getItemSizeMax
	 * @return
	 */
	public int getItemSizeMax();

	/**
	 * getItemCount
	 * @return
	 */
	public int getItemCount();

	/**
	 * addItem
	 * @param slot
	 * @param aitem
	 * @return
	 */
	public boolean addItem(int slot, IItem aitem);

	/**
	 * addItem
	 * @param aitem
	 * @return
	 */
	public boolean addItem(IItem aitem);

	/**
	 * isSlotFree
	 * @param slot
	 * @return
	 */
	public boolean isSlotFree(int slot);
}