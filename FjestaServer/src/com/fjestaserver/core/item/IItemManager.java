/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.item;

import com.fjestacore.common.inventory.InventoryType;

/**
 * IItemManager
 * @author FantaBlueMystery
 */
public interface IItemManager {

	/**
	 * addInventory
	 * @param inv
	 */
	public void addInventory(IItemInventory inv);

	/**
	 * createInventory
	 * @param characterHandle
	 * @param type
	 * @return
	 */
	public IItemInventory createInventory(long characterHandle, InventoryType type);

	/**
	 * getInventory
	 * @param characterHandle
	 * @param type
	 * @return
	 */
	public IItemInventory getInventory(long characterHandle, InventoryType type);
}