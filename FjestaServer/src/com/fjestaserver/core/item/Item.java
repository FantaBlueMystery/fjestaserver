/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.item;

/**
 * Item
 * @author FantaBlueMystery
 */
public class Item extends com.fjestacore.common.item.Item implements IItem {

	/**
	 * unique id
	 */
	protected long _uniqueId = 0;

	/**
	 * getUId
	 * @return
	 */
	@Override
	public long getUId() {
		return this._uniqueId;
	}
}