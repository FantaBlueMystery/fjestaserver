/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.item;

import com.fjestacore.common.briefinfo.DropedItem;
import com.fjestacore.common.briefinfo.DropedItemState;
import com.fjestacore.common.position.PositionXY;
import com.fjestacore.common.position.PositionXYR;
import com.fjestaserver.core.map.IMap;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.map.MapObjectType;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ItemDrop
 * @author FantaBlueMystery
 */
public class ItemDrop extends DropedItem implements IMapObject, IItemDroped {

	/**
	 * map
	 */
	protected IMap _map = null;

	/**
	 * item
	 */
	protected IItem _item = null;

	/**
	 * DropedItemServer
	 */
	public ItemDrop() {}

	/**
	 * DropedItemServer
	 * @param dropByMapObject
	 * @param itemid
	 * @param state
	 */
	public ItemDrop(IMapObject dropByMapObject, int itemid, DropedItemState state) {
		this._itemid = itemid;

		try {
			this._location = (PositionXY) dropByMapObject.getPosition().clone();
		}
		catch( CloneNotSupportedException ex ) {
			Logger.getLogger(ItemDrop.class.getName()).log(Level.SEVERE, null, ex);
		}

		this._dropmobhandle = dropByMapObject.getObjectHandle();
		this._attr			= state;
	}

	/**
	 * DropedItemServer
	 * @param pos
	 * @param itemid
	 * @param state
	 */
	public ItemDrop(PositionXY pos, int itemid, DropedItemState state) {
		this._itemid		= itemid;
		this._location		= pos;
		this._dropmobhandle = 65535;
		this._attr			= state;
	}

	/**
	 * ItemDrop
	 * @param item
	 * @param pos
	 * @param state
	 */
	public ItemDrop(IItem item, PositionXY pos, DropedItemState state) {
		this._item			= item;
		this._itemid		= item.getId();
		this._location		= pos;
		this._dropmobhandle = 65535;
		this._attr			= state;
	}

	/**
	 * getType
	 * @return
	 */
	@Override
	public MapObjectType getType() {
		return MapObjectType.ITEM;
	}

	/**
	 * getPosition
	 * @return
	 */
	@Override
	public PositionXY getPosition() {
		return this._location;
	}

	/**
	 * setMap
	 * @param map
	 */
	@Override
	public void setMap(IMap map) {
		this._map = map;
	}

	/**
	 * getMap
	 * @return
	 */
	@Override
	public IMap getMap() {
		return this._map;
	}

	/**
	 * getItem
	 * @return
	 */
	@Override
	public IItem getItem() {
		return this._item;
	}

	/**
	 * setItem
	 * @param item
	 */
	@Override
	public void setItem(IItem item) {
		this._item = item;
	}
}