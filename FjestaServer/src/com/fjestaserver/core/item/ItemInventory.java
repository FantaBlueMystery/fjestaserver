/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.item;

import com.fjestacore.common.inventory.Inventory;
import com.fjestacore.common.inventory.InventoryType;
import com.fjestacore.common.item.Item;

/**
 * ItemInventory
 * @author FantaBlueMystery
 */
public class ItemInventory extends Inventory implements IItemInventory {

	/**
	 * character handle
	 */
	protected long _characterHandle = 0;

	/**
	 * max size
	 */
	protected int _maxSize = 0;

	/**
	 * ItemInventory
	 * @param characterHandle
	 * @param type
	 */
	public ItemInventory(long characterHandle, InventoryType type) {
		this._characterHandle	= characterHandle;
		this._type				= type;
	}

	/**
	 * getCharacterHandle
	 * character owner of inventory
	 * @return
	 */
	@Override
	public long getCharacterHandle() {
		return this._characterHandle;
	}

	/**
	 * getItem
	 * @param slot
	 * @return
	 */
	public IItem getItem(int slot) {
		Item i = this._items.get(slot);

		if( i != null ) {
			return (IItem) i;
		}

		return null;
	}

	/**
	 * getItemSizeMax
	 * @return
	 */
	@Override
	public int getItemSizeMax() {
		return this._maxSize;
	}

	/**
	 * getItemCount
	 * @return
	 */
	@Override
	public int getItemCount() {
		return this._items.size();
	}

	/**
	 * addItem
	 * @param slot
	 * @param aitem
	 * @return
	 */
	@Override
	public boolean addItem(int slot, IItem aitem) {
		if( this.isSlotFree(slot) ) {
			this._items.put(slot, (Item) aitem);

			return true;
		}

		return false;
	}

	/**
	 * addItem
	 * @param aitem
	 * @return
	 */
	@Override
	public boolean addItem(IItem aitem) {
		for( int i=0; i<this._maxSize; i++ ) {
			if( this.isSlotFree(i) ) {
				this._items.put(i, (Item) aitem);

				return true;
			}
		}

		return false;
	}

	/**
	 * isSlotFree
	 * @param slot
	 * @return
	 */
	@Override
	public boolean isSlotFree(int slot) {
		return !this._items.containsKey(slot);
	}
}