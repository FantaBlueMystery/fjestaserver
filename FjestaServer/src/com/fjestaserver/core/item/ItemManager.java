/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.item;

import com.fjestacore.common.inventory.InventoryType;
import com.fjestacore.core.list.MKey2;
import java.util.HashMap;
import java.util.Map;

/**
 * ItemManager
 * @author FantaBlueMystery
 */
public class ItemManager implements IItemManager {

	/**
	 * item manager
	 */
	static protected IItemManager _instance;

	/**
	 * getInstance
	 * return instance of item manager
	 * @return
	 */
	static public IItemManager getInstance() {
		if( ItemManager._instance == null ) {
			ItemManager._instance = new ItemManager();
		}

		return ItemManager._instance;
	}

	/**
	 * list (memory)
	 */
	protected Map<MKey2, IItemInventory> _list = new HashMap<>();

	/**
	 * addInventory
	 * @param inv
	 */
	@Override
	public void addInventory(IItemInventory inv) {
		this._list.put(new MKey2(inv.getCharacterHandle(), inv.getType()), inv);
	}

	/**
	 * createInventory
	 * @param characterHandle
	 * @param type
	 * @return
	 */
	@Override
	public IItemInventory createInventory(long characterHandle, InventoryType type) {
		IItemInventory ii = new ItemInventory(characterHandle, type);

		this.addInventory(ii);

		return ii;
	}

	/**
	 * getInventory
	 * @param characterHandle
	 * @param type
	 * @return
	 */
	@Override
	public IItemInventory getInventory(long characterHandle, InventoryType type) {
		return this._list.get(new MKey2(characterHandle, type));
	}
}