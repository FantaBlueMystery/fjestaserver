/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.map;

import com.fjestacore.common.map.FieldMapType;
import com.fjestacore.common.map.MapInfo;
import com.fjestaserver.zone.ServerZone;
import java.util.ArrayList;

/**
 * IMap
 * @author FantaBlueMystery
 */
public interface IMap {

	/**
	 * getUniqueId
	 * @return
	 */
	public int getUniqueId();

	/**
	 * setUniqueId
	 * @param uid
	 */
	public void setUniqueId(int uid);

	/**
	 * getOwnerObject
	 * return object that created the map
	 * @return
	 */
	public Object getOwnerObject();

	/**
	 * setOwnerObject
	 * set the object that created the map
	 * @param owner
	 */
	public void setOwnerObject(Object owner);

	/**
	 * getServer
	 * return server zone
	 * @return
	 */
	public ServerZone getServer();

	/**
	 * setServer
	 * set server zone
	 * @param sz
	 */
	public void setServer(ServerZone sz);

	/**
	 * getMapType
	 * @return
	 */
	public FieldMapType getMapType();

	/**
	 * getMapInfo
	 * @return
	 */
	public MapInfo getMapInfo();

	/**
	 * getObjectList
	 * @return
	 */
	public MapObjectList getObjectList();

	/**
	 * addObject
	 * @param mo
	 */
	public void addObject(IMapObject mo);

	/**
	 * removeObject
	 * @param mo
	 */
	public void removeObject(IMapObject mo);

	/**
	 * removeObject
	 * @param handle
	 */
	public void removeObject(int handle);

	/**
	 * getMobs
	 * @return
	 */
	public ArrayList<IMapObject> getMobs();
}