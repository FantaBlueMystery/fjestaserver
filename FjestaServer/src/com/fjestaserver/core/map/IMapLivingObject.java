/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.map;

import com.fjestacore.common.move.MoveFromTo;
import com.fjestacore.common.position.PositionXY;
import com.fjestaserver.core.target.ITarget;

/**
 * IMapLivingObject
 * @author FantaBlueMystery
 */
public interface IMapLivingObject extends IMapObject, ITarget {

	/**
	 * moveRun
	 * @param move
	 */
	public void moveRun(MoveFromTo move);

	/**
	 * moveWalk
	 * @param move
	 */
	public void moveWalk(MoveFromTo move);

	/**
	 * moveStop
	 * @param pos
	 */
	public void moveStop(PositionXY pos);
}