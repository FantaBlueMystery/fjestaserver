/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.map;

import com.fjestacore.common.map.FieldMapType;
import com.fjestacore.common.map.MapInfo;
import java.util.ArrayList;

/**
 * IMapManager
 * @author FantaBlueMystery
 */
public interface IMapManager {

	/**
	 * getMap
	 * @param uid
	 * @return
	 */
	public IMap getMap(int uid);

	/**
	 * getMapByOwner
	 * @param o
	 * @return
	 */
	public IMap getMapByOwner(Object o);

	/**
	 * addMap
	 * @param mpa
	 */
	public void addMap(IMap mpa);

	/**
	 * getMaps
	 * @return
	 */
	public ArrayList<IMap> getMaps();

	/**
	 * getMaps
	 * @param type
	 * @return
	 */
	public ArrayList<IMap> getMaps(FieldMapType type);

	/**
	 * createMap
	 * create and add IMap to list
	 * @param mi
	 * @return
	 */
	public IMap createMap(MapInfo mi);
}