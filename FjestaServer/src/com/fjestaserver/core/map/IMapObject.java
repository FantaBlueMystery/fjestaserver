/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.map;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.handle.IObjectHandle;

/**
 * IWorldMapObject
 * @author FantaBlueMystery
 */
public interface IMapObject extends IObjectHandle {

	/**
	 * setMap
	 * @param map
	 */
	public void setMap(IMap map);

	/**
	 * getMap
	 * @return
	 */
	public IMap getMap();

	/**
	 * getType
	 * @return
	 */
	public MapObjectType getType();

	/**
	 * getPosition
	 * @return
	 */
	public PositionXY getPosition();
}