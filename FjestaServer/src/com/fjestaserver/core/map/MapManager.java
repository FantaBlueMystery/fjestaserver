/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.map;

import com.fjestacore.common.map.FieldMapType;
import com.fjestacore.common.map.MapInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * MapManager
 * @author FantaBlueMystery
 */
public class MapManager implements IMapManager {

	/**
	 * map manager
	 */
	static protected IMapManager _instance;

	/**
	 * getInstance
	 * return instance of map manager
	 * @return
	 */
	static public IMapManager getInstance() {
		if( MapManager._instance == null ) {
			MapManager._instance = new MapManager();
		}

		return MapManager._instance;
	}

	/**
	 * list (memory)
	 */
	protected Map<Integer, IMap> _list = new HashMap<>();

	/**
	 * getMap
	 * @param uid
	 * @return
	 */
	@Override
	public IMap getMap(int uid) {
		return this._list.get(uid);
	}

	/**
	 * getMapByOwner
	 * @param o
	 * @return
	 */
	@Override
	public IMap getMapByOwner(Object o) {
		for( Map.Entry<Integer, IMap> entry : this._list.entrySet() ) {
			Object owner = entry.getValue().getOwnerObject();

			if( (owner != null) && owner.equals(o) ) {
				return entry.getValue();
			}
		}

		return null;
	}

	/**
	 * addMap
	 * @param mpa
	 */
	@Override
	public void addMap(IMap mpa) {
		if( mpa.getUniqueId() == 0 ) {
			mpa.setUniqueId(this._list.size()+1);
		}

		if( !this._list.containsKey(mpa.getUniqueId()) ) {
			this._list.put(mpa.getUniqueId(), mpa);
		}
	}

	/**
	 * getMaps
	 * @return
	 */
	@Override
	public ArrayList<IMap> getMaps() {
		ArrayList<IMap> list = new ArrayList();

		for( Map.Entry<Integer, IMap> entry : this._list.entrySet() ) {
			list.add(entry.getValue());
		}

		return list;
	}

	/**
	 * getMaps
	 * @param type
	 * @return
	 */
	@Override
	public ArrayList<IMap> getMaps(FieldMapType type) {
		ArrayList<IMap> list = new ArrayList();

		for( Map.Entry<Integer, IMap> entry : this._list.entrySet() ) {
			if( entry.getValue().getMapType() == type ) {
				list.add(entry.getValue());
			}
		}

		return list;
	}

	/**
	 * createMap
	 * create and add IMap to list
	 * @param mi
	 * @return
	 */
	@Override
	public IMap createMap(MapInfo mi) {
		IMap map = new MapObject(mi);

		this.addMap(map);

		return map;
	}
}