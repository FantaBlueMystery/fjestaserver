/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.map;

import com.fjestacore.common.briefinfo.BriefInfoCharacter;
import com.fjestacore.common.briefinfo.BriefInfoDelete;
import com.fjestacore.common.briefinfo.ItemOnField;
import com.fjestacore.common.map.FieldMapType;
import com.fjestacore.common.map.MapInfo;
import com.fjestacore.common.mob.Mob;
import com.fjestacore.common.mob.MobList;
import com.fjestacore.packet.briefinfo.NC_BRIEFINFO_DROPEDITEM_CMD;
import com.fjestacore.packet.briefinfo.NC_BRIEFINFO_LOGINCHARACTER_CMD;
import com.fjestacore.packet.briefinfo.NC_BRIEFINFO_REGENMOB_CMD;
import com.fjestacore.packet.briefinfo.NC_BRIEFINFO_BRIEFINFODELETE_CMD;
import com.fjestacore.packet.briefinfo.NC_BRIEFINFO_CHARACTER_CMD;
import com.fjestacore.packet.briefinfo.NC_BRIEFINFO_MOB_CMD;
import com.fjestacore.packet.briefinfo.NC_BRIEFINFO_ITEMONFIELD_CMD;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.item.ItemDrop;
import com.fjestaserver.core.mob.DefaultMob;
import com.fjestaserver.core.mob.NpcPlayer;
import com.fjestaserver.zone.ServerZone;
import java.util.ArrayList;

/**
 * WorldMap
 * @author FantaBlueMystery
 */
public class MapObject implements IMap {

	/**
	 * uid
	 */
	protected int _uid = 0;

	/**
	 * server zone reference
	 */
	protected ServerZone _serverZone = null;

	/**
	 * owner
	 */
	protected Object _owner = null;

	/**
	 * map type
	 */
	protected FieldMapType _mapType = FieldMapType.FMT_NORMAL;

	/**
	 * map info
	 */
	protected MapInfo _mapInfo = null;

	/**
	 * map object list
	 */
	protected MapObjectList _objectList = new MapObjectList();

	/**
	 * Map
	 * @param mi
	 */
	public MapObject(MapInfo mi) {
		this._mapInfo = mi;
	}

	/**
	 * getUniqueId
	 * @return
	 */
	@Override
	public int getUniqueId() {
		return this._uid;
	}

	/**
	 * setUniqueId
	 * @param uid
	 */
	@Override
	public void setUniqueId(int uid) {
		this._uid = uid;
	}

	/**
	 * getServer
	 * reference to server zone
	 * @return
	 */
	@Override
	public ServerZone getServer() {
		return this._serverZone;
	}

	/**
	 * setServer
	 * reference to server zone
	 * @param sz
	 */
	@Override
	public void setServer(ServerZone sz) {
		this._serverZone = sz;
	}

	/**
	 * getOwnerObject
	 * @return
	 */
	@Override
	public Object getOwnerObject() {
		return this._owner;
	}

	/**
	 * setOwnerObject
	 * @param owner
	 */
	@Override
	public void setOwnerObject(Object owner) {
		this._owner = owner;
	}

	/**
	 * getMapInfo
	 * @return
	 */
	@Override
	public MapInfo getMapInfo() {
		return this._mapInfo;
	}

	/**
	 * getObjectList
	 * @return
	 */
	@Override
	public MapObjectList getObjectList() {
		return this._objectList;
	}

	/**
	 * getMapType
	 * @return
	 */
	@Override
	public FieldMapType getMapType() {
		return this._mapType;
	}

	/**
	 * addObject
	 * @param mo
	 */
	@Override
	public void addObject(IMapObject mo) {
		mo.setMap(this);

		this._objectList.addObject(mo);

		if( this._serverZone == null ) {
			return;
		}

		switch( mo.getType() ) {
			case PLAYER:
				ICharacterServer cs = (ICharacterServer) mo;

				if( cs.getClientZone() == null ) {
					return;
				}

				// -------------------------------------------------------------

				cs.getClientZone().getServer().writePacketInRange(
					new NC_BRIEFINFO_LOGINCHARACTER_CMD(cs.getLoginCharacter()),
					cs.getPosition(),
					cs.getClientZone().getSession().getId()
					);

				// -------------------------------------------------------------

				BriefInfoCharacter bic = this._objectList.getPlayerInfos(cs);

				if( bic.getCharacterObjects().size() > 0 ) {
					cs.getClientZone().writePacket(new NC_BRIEFINFO_CHARACTER_CMD(bic));
				}

				// -------------------------------------------------------------

				MobList ml = this._objectList.getMobList();

				if( ml.getListSize() > 0 ) {
					cs.getClientZone().writePacket(new NC_BRIEFINFO_MOB_CMD(ml));
				}

				// -------------------------------------------------------------

				ItemOnField iof = this._objectList.getItemOnField();

				if( iof.getListSize() > 0 ) {
					cs.getClientZone().writePacket(new NC_BRIEFINFO_ITEMONFIELD_CMD(iof));
				}

				break;

			case MOB:
				if( mo instanceof Mob ) {
					DefaultMob dm = (DefaultMob) mo;

					this._serverZone.writePacketInRange(
						new NC_BRIEFINFO_REGENMOB_CMD(dm),
						dm.getPosition()
						);
				}
				break;

			case NPCPLAYER:
				if( mo instanceof NpcPlayer ) {
					NpcPlayer npcp = (NpcPlayer) mo;

					this._serverZone.writePacketInRange(
						new NC_BRIEFINFO_LOGINCHARACTER_CMD(npcp),
						npcp.getPosition()
						);
				}
				break;

			case ITEM:
				if( mo instanceof ItemDrop ) {
					ItemDrop di = (ItemDrop) mo;

					this._serverZone.writePacketInRange(
						new NC_BRIEFINFO_DROPEDITEM_CMD(di),
						di.getPosition()
						);
				}
				break;
		}
	}

	/**
	 * removeObject
	 * @param mo
	 */
	@Override
	public void removeObject(IMapObject mo) {
		mo.setMap(null);

		this._objectList.removeByObjectId(mo.getObjectHandle(), mo.getType());

		if( this._serverZone == null ) {
			return;
		}

		switch( mo.getType() ) {
			case PLAYER:
				// TODO

			default:
				this._serverZone.writePacketBroadcast(
					new NC_BRIEFINFO_BRIEFINFODELETE_CMD(new BriefInfoDelete(mo.getObjectHandle()))
					);
		}
	}

	/**
	 * removeObject
	 * @param handle
	 */
	@Override
	public void removeObject(int handle) {
		IMapObject mo = this._objectList.getByObjectId(handle);

		this.removeObject(mo);
	}

	/**
	 * getMobs
	 * @return
	 */
	@Override
	public ArrayList<IMapObject> getMobs() {
		return this._objectList.getMobs();
	}
}
