/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.map;

import com.fjestacore.common.briefinfo.BriefInfoCharacter;
import com.fjestacore.common.briefinfo.DropedItem;
import com.fjestacore.common.briefinfo.ItemOnField;
import com.fjestacore.common.briefinfo.LoginCharacter;
import com.fjestacore.common.mob.Mob;
import com.fjestacore.common.mob.MobList;
import java.util.HashMap;
import java.util.Map;
import com.fjestacore.core.list.MKey2;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.handle.HandleManager;
import com.fjestaserver.core.mob.NpcPlayer;
import java.util.ArrayList;

/**
 * MapObjectList
 * @author FantaBlueMystery
 */
public class MapObjectList {

	/**
	 * list
	 */
	protected Map<MKey2, IMapObject> _list = new HashMap<>();

	/**
	 * addObject
	 * @param object
	 */
	public void addObject(IMapObject object) {
		if( object.getObjectHandle() == 0 ) {
			object.setObjectHandle(HandleManager.getInstance().getFreeObjectHandle());
		}

		this._list.put(new MKey2(object.getObjectHandle(), object.getType()), object);
	}

	/**
	 * getByObjectId
	 * @param id
	 * @return
	 */
	public IMapObject getByObjectId(int id) {
		for( MapObjectType atype: MapObjectType.values() ) {
			IMapObject tmo = this.getByObjectId(id, atype);

			if( tmo != null ) {
				return tmo;
			}
		}

		return null;
	}

	/**
	 * getByObjectId
	 * @param id
	 * @param type
	 * @return
	 */
	public IMapObject getByObjectId(int id, MapObjectType type) {
		return this._list.get(new MKey2(id, type));
	}

	/**
	 * removeByObjectId
	 * @param id
	 * @param type
	 */
	public void removeByObjectId(int id,  MapObjectType type) {
		this._list.remove(new MKey2(id, type));
	}

	/**
	 * getPlayer
	 * @param id
	 * @return
	 */
	public IMapObject getPlayer(int id) {
		return this.getByObjectId(id, MapObjectType.PLAYER);
	}

	/**
	 * removePlayer
	 * @param id
	 */
	public void removePlayer(int id) {
		this.removeByObjectId(id, MapObjectType.PLAYER);
	}

	/**
	 * getPlayerInfos
	 * @return
	 */
	public BriefInfoCharacter getPlayerInfos() {
		return this.getPlayerInfos(null);
	}

	/**
	 * getPlayerInfos
	 * @param byCharacter
	 * @return
	 */
	public BriefInfoCharacter getPlayerInfos(ICharacterServer byCharacter) {
		ArrayList<LoginCharacter> characters = new ArrayList<>();

		for( Map.Entry<MKey2, IMapObject> entry : this._list.entrySet() ) {
			if( entry.getKey().getKey2() == MapObjectType.PLAYER ) {
				ICharacterServer cs = (ICharacterServer) entry.getValue();

				if( byCharacter != null ) {
					// sort out player it self
					if( cs.getCharacterHandle() == byCharacter.getCharacterHandle()) {
						continue;
					}

					if( cs.getPosition().inRange(byCharacter.getPosition()) ) {
						characters.add(cs.getLoginCharacter());
					}
				}
				else {
					characters.add(cs.getLoginCharacter());
				}
			}
			else if( entry.getKey().getKey2() == MapObjectType.NPCPLAYER ) {
				NpcPlayer npcp = (NpcPlayer) entry.getValue();

				characters.add(npcp);
			}
		}

		return new BriefInfoCharacter(characters);
	}

	/**
	 * getMob
	 * @param id
	 * @return
	 */
	public IMapObject getMob(int id) {
		return this.getByObjectId(id, MapObjectType.MOB);
	}

	/**
	 * getMobs
	 * @return
	 */
	public ArrayList<IMapObject> getMobs() {
		ArrayList<IMapObject> olist = new ArrayList<>();

		for( Map.Entry<MKey2, IMapObject> entry : this._list.entrySet() ) {
			if( entry.getKey().getKey2() == MapObjectType.MOB ) {
				olist.add(entry.getValue());
			}
		}

		return olist;
	}

	/**
	 * getMobList
	 * @return
	 */
	public MobList getMobList() {
		MobList mlist = new MobList();

		for( Map.Entry<MKey2, IMapObject> entry : this._list.entrySet() ) {
			if( entry.getKey().getKey2() == MapObjectType.MOB ) {
				mlist.add((Mob) entry.getValue());
			}
		}

		return mlist;
	}

	/**
	 * getNPCPlayer
	 * @param id
	 * @return
	 */
	public IMapObject getNPCPlayer(int id) {
		return this.getByObjectId(id, MapObjectType.NPCPLAYER);
	}

	/**
	 * getItem
	 * @param id
	 * @return
	 */
	public IMapObject getItem(int id) {
		return this.getByObjectId(id, MapObjectType.ITEM);
	}

	/**
	 * getItemOnField
	 * @return
	 */
	public ItemOnField getItemOnField() {
		ItemOnField iof = new ItemOnField();

		for( Map.Entry<MKey2, IMapObject> entry : this._list.entrySet() ) {
			if( entry.getKey().getKey2() == MapObjectType.ITEM ) {
				IMapObject mo = entry.getValue();

				if( mo instanceof DropedItem ) {
					iof.addItem((DropedItem) mo);
				}
			}
		}

		return iof;
	}
}