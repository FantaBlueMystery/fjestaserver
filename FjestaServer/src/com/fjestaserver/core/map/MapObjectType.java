/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.map;

/**
 * MapObjectType
 * @author FantaBlueMystery
 */
public enum  MapObjectType {
	PLAYER(1),
	NPCPLAYER(2),
	MOB(30),
	ITEM(41)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * ItemOptionType
	 * @param value
	 */
	private MapObjectType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}