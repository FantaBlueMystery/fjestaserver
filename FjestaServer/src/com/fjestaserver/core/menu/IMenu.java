/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.menu;

import com.fjestaserver.core.net.Client;

/**
 * IMenu
 * @author FantaBlueMystery
 */
public interface IMenu {

	/**
	 * onMenuAction
	 * @param reply
	 * @param client
	 */
	public void onMenuAction(int reply, Client client);
}