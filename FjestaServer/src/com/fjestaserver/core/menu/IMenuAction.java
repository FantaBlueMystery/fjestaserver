/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.menu;

import com.fjestacore.common.servermenu.ServerMenu;
import com.fjestacore.common.servermenu.ServerMenuEntry;
import com.fjestaserver.core.net.Client;

/**
 * IMenuAction
 * @author FantaBlueMystery
 */
public interface IMenuAction {

	/**
	 * action
	 * @param sm
	 * @param sme
	 * @param client
	 */
	public void action(ServerMenu sm, ServerMenuEntry sme, Client client);
}