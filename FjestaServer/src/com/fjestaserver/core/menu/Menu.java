/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.menu;

import com.fjestacore.common.servermenu.ServerMenu;
import com.fjestacore.common.servermenu.ServerMenuEntry;
import com.fjestaserver.core.net.Client;

/**
 * Menu
 * @author FantaBlueMystery
 */
public class Menu extends ServerMenu implements IMenu {

	/**
	 * Menu
	 * @param title
	 * @param limitRange
	 */
	public Menu(String title, int limitRange) {
		this._title			= title;
		this._limitRange	= limitRange;
	}

	/**
	 * onMenuAction
	 * @param reply
	 * @param client
	 */
	@Override
	public void onMenuAction(int reply, Client client) {
		for( ServerMenuEntry sme: this._menu ) {
			if( sme.getReply() == reply ) {
				IMenuAction sma = ((MenuEntry)sme).getAction();

				if( sma != null ) {
					sma.action(this, sme, client);
				}

				break;
			}
		}
	}
}