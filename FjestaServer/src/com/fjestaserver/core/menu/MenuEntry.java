/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.menu;

import com.fjestacore.common.servermenu.ServerMenuEntry;

/**
 * MenuEntry
 * @author FantaBlueMystery
 */
public class MenuEntry extends ServerMenuEntry {

	/**
	 * action
	 */
	protected IMenuAction _action = null;

	/**
	 * MenuEntry
	 * @param reply
	 * @param aString
	 * @param action
	 */
	public MenuEntry(int reply, String aString, IMenuAction action) {
		this._reply		= reply;
		this._aString	= aString;
		this._action	= action;
	}

	/**
	 * getAction
	 * @return
	 */
	public IMenuAction getAction() {
		return this._action;
	}
}