/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.mob;

import com.fjestacore.common.act.SomeoneStop;
import com.fjestacore.common.bat.TargetInfo;
import com.fjestacore.common.character.CharBars;
import com.fjestacore.common.character.CharBarsMax;
import com.fjestacore.common.mob.Mob;
import com.fjestacore.common.mob.MobInfo;
import com.fjestacore.common.move.MoveFromTo;
import com.fjestacore.common.move.SomeoneMoveFromTo;
import com.fjestacore.common.position.PositionXY;
import com.fjestacore.packet.act.NC_ACT_SOMEONEMOVERUN_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEMOVEWALK_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONESTOP_CMD;
import com.fjestaserver.core.map.IMap;
import com.fjestaserver.core.map.IMapLivingObject;
import com.fjestaserver.core.map.MapObjectType;

/**
 * MobServer
 * @author FantaBlueMystery
 */
public class DefaultMob extends Mob implements IMapLivingObject {

	/**
	 * map
	 */
	protected IMap _map = null;

	/**
	 * bars
	 */
	protected CharBars _bars = new CharBars();

	/**
	 * bars max
	 */
	protected CharBarsMax _barsMax = new CharBarsMax();

	/**
	 * move speed
	 */
	protected int _moveSpeed = 100;

	/**
	 * DefaultMob
	 */
	public DefaultMob() {}

	/**
	 * getBars
	 * @return
	 */
	public CharBars getBars() {
		return this._bars;
	}

	/**
	 * getBarsMax
	 * @return
	 */
	public CharBarsMax getBarsMax() {
		return this._barsMax;
	}

	/**
	 * getType
	 * @return
	 */
	@Override
	public MapObjectType getType() {
		return MapObjectType.MOB;
	}

	/**
	 * getTargetInfo
	 * @return
	 */
	@Override
	public TargetInfo getTargetInfo() {
		TargetInfo info = new TargetInfo();

		info.setFlag(0x80);
		info.setObjectHandle(this.getObjectHandle());
		info.setTargetHp(this._bars.getHp());
		info.setTargetSp(this._bars.getSp());
		info.setTargetLp(this._bars.getLp());
		info.setTargetMaxHp(this._barsMax.getHp());
		info.setTargetMaxSp(this._barsMax.getSp());
		info.setTargetMaxLp(this._barsMax.getLp());

		MobInfo mi = this.getMobInfo();

		if( mi != null ) {
			info.setTargetLevel(mi.getLevel());
		}

		return info;
	}

	/**
	 * setMap
	 * @param map
	 */
	@Override
	public void setMap(IMap map) {
		this._map = map;
	}

	/**
	 * getMap
	 * @return
	 */
	@Override
	public IMap getMap() {
		return this._map;
	}

	/**
	 * moveRun
	 * @param move
	 */
	@Override
	public void moveRun(MoveFromTo move) {
		this.getPosition().setPosition(move.getTo());

		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(new NC_ACT_SOMEONEMOVERUN_CMD(
			new SomeoneMoveFromTo(this.getObjectHandle(), move, this._moveSpeed)),
			this.getPosition()
			);
	}

	/**
	 * moveWalk
	 * @param move
	 */
	@Override
	public void moveWalk(MoveFromTo move) {
		this.getPosition().setPosition(move.getTo());

		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(new NC_ACT_SOMEONEMOVEWALK_CMD(
			new SomeoneMoveFromTo(this.getObjectHandle(), move, this._moveSpeed)),
			this.getPosition()
			);
	}

	/**
	 * moveStop
	 * @param pos
	 */
	@Override
	public void moveStop(PositionXY pos) {
		this.getPosition().setPosition(pos);

		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(
			new NC_ACT_SOMEONESTOP_CMD(new SomeoneStop(
				this.getObjectHandle(),
				pos
				)),
			this.getPosition()
			);
	}
}