/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.mob;

import com.fjestacore.packet.menu.NC_MENU_SERVERMENU_REQ;
import com.fjestaserver.core.menu.Menu;
import com.fjestaserver.core.mob.event.IMobEventNPCClick;
import com.fjestaserver.zone.ClientZone;

/**
 * NpcMob
 * @author FantaBlueMystery
 */
public class NpcMob extends DefaultMob implements IMobEventNPCClick {

	/**
	 * menu
	 */
	protected Menu _menu = null;

	/**
	 * onNPCClick
	 * @param client
	 */
	@Override
	public void onNPCClick(ClientZone client) {
		if( this._menu != null ) {
			// update handle + position
			this._menu.setObjectHandle(this._handle);
			this._menu.setPosition(this._pos);

			client.getSession().setUsedMenu(this._menu);

			// send to client
			client.writePacket(new NC_MENU_SERVERMENU_REQ(this._menu));
		}
	}
}