/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.mob;

import com.fjestacore.common.act.SomeoneEmoticon;
import com.fjestacore.common.act.SomeoneEmoticonStop;
import com.fjestacore.common.act.SomeoneJump;
import com.fjestacore.common.act.SomeoneStop;
import com.fjestacore.common.bat.TargetInfo;
import com.fjestacore.common.briefinfo.LoginCharacter;
import com.fjestacore.common.character.CharParameterData;
import com.fjestacore.common.move.MoveFromTo;
import com.fjestacore.common.move.SomeoneMoveFromTo;
import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.packet.act.NC_ACT_SOMEEONEJUMP_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEEMOTICONSTOP_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEEMOTICON_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEMOVERUN_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONEMOVEWALK_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONESTOP_CMD;
import com.fjestaserver.core.map.IMap;
import com.fjestaserver.core.map.MapObjectType;
import com.fjestaserver.core.mob.event.IMobEventNPCClick;
import com.fjestaserver.zone.ClientZone;
import com.fjestaserver.core.characters.ICharacterAction;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * NpcPlayer
 * @author FantaBlueMystery
 */
public class NpcPlayer extends LoginCharacter implements ICharacterAction, IMobEventNPCClick {

	/**
	 * map
	 */
	protected IMap _map = null;

	/**
	 * parameter
	 */
	protected CharParameterData _parameter = new CharParameterData();

	/**
	 * move speed
	 */
	protected int _moveSpeed = 100;

	/**
	 * NpcPlayer
	 */
	public NpcPlayer() {}

	/**
	 * NpcPlayer
	 * @param hexstr
	 */
	public NpcPlayer(String hexstr) {
		this._setByHex(hexstr, true);
	}

	/**
	 * NpcPlayer
	 * @param hexstr
	 * @param useNpcTag
	 */
	public NpcPlayer(String hexstr, boolean useNpcTag) {
		this._setByHex(hexstr, useNpcTag);
	}

	/**
	 * _setByHex
	 * @param hexstr
	 * @param useNpcTag
	 */
	protected void _setByHex(String hexstr, boolean useNpcTag) {
		try {
			this.read(HexString.hexStringToByteArray(hexstr));
			this.setObjectHandle(0);	// reset handle

			String name = this._name.getContent();

			if( useNpcTag ) {
				name = "[NPC]" + name;
			}

			this._name.setContent(name);
		}
		catch( IOException ex ) {
			Logger.getLogger(NpcPlayer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * getType
	 * @return
	 */
	@Override
	public MapObjectType getType() {
		return MapObjectType.NPCPLAYER;
	}

	/**
	 * onNPCClick
	 * @param client
	 */
	@Override
	public void onNPCClick(ClientZone client) {}

	/**
	 * getTargetInfo
	 * @return
	 */
	@Override
	public TargetInfo getTargetInfo() {
		TargetInfo info = new TargetInfo();

		info.setFlag(0x80);
		info.setObjectHandle(this.getObjectHandle());
		info.setTargetHp(this._bars.getHp());
		info.setTargetSp(this._bars.getSp());
		info.setTargetLp(this._bars.getLp());
		info.setTargetMaxHp(this._parameter.getBarsMax().getHp());
		info.setTargetMaxSp(this._parameter.getBarsMax().getSp());
		info.setTargetMaxLp(this._parameter.getBarsMax().getLp());
		info.setTargetLevel(this._level);

		return info;
	}

	/**
	 * setMap
	 * @param map
	 */
	@Override
	public void setMap(IMap map) {
		this._map = map;
	}

	/**
	 * getMap
	 * @return
	 */
	@Override
	public IMap getMap() {
		return this._map;
	}

	/**
	 * useEmote
	 * @param id
	 */
	@Override
	public void useEmote(int id) {
		this._emoticon.setEmoticonId(id);

		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(
			new NC_ACT_SOMEONEEMOTICON_CMD(
				new SomeoneEmoticon(this.getObjectHandle(), this.getEmoticon().getEmoticonId())
			),
			this.getPosition()
			);
	}

	/**
	 * useJump
	 * @param byClient
	 */
	@Override
	public void useJump(boolean byClient) {
		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(
			new NC_ACT_SOMEEONEJUMP_CMD(
				new SomeoneJump(this.getObjectHandle())
			),
			this.getPosition()
			);
	}

	/**
	 * stopEmote
	 */
	@Override
	public void stopEmote() {
		this._emoticon.setEmoticonId(0);

		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(
			new NC_ACT_SOMEONEEMOTICONSTOP_CMD(
				new SomeoneEmoticonStop(this.getObjectHandle(), this.getEmoticon())
			),
			this.getPosition()
			);
	}

	/**
	 * moveRun
	 * @param move
	 */
	@Override
	public void moveRun(MoveFromTo move) {
		this.getPosition().setPosition(move.getTo());

		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(new NC_ACT_SOMEONEMOVERUN_CMD(
			new SomeoneMoveFromTo(this.getObjectHandle(), move, this._moveSpeed)),
			this.getPosition()
			);
	}

	/**
	 * moveWalk
	 * @param move
	 */
	@Override
	public void moveWalk(MoveFromTo move) {
		this.getPosition().setPosition(move.getTo());

		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(new NC_ACT_SOMEONEMOVEWALK_CMD(
			new SomeoneMoveFromTo(this.getObjectHandle(), move, this._moveSpeed)),
			this.getPosition()
			);
	}

	/**
	 * moveStop
	 * @param pos
	 */
	@Override
	public void moveStop(PositionXY pos) {
		this.getPosition().setPosition(pos);

		if( this._map == null ) {
			return;
		}

		this._map.getServer().writePacketInRange(
			new NC_ACT_SOMEONESTOP_CMD(new SomeoneStop(
				this.getObjectHandle(),
				pos
				)),
			this.getPosition()
			);
	}

	/**
	 * useItem
	 * @param slot
	 * @param lot
	 */
	@Override
	public void useItem(int slot, int lot) {}

	/**
	 * pickItem
	 * @param itemHandle
	 */
	@Override
	public void pickItem(int itemHandle) {}
}