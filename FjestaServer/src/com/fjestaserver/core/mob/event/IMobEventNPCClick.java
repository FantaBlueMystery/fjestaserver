/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.mob.event;

import com.fjestaserver.zone.ClientZone;

/**
 * IMobEventNPCClick
 * @author FantaBlueMystery
 */
public interface IMobEventNPCClick {

	/**
	 * onNPCClick
	 * @param client
	 */
	public void onNPCClick(ClientZone client);
}