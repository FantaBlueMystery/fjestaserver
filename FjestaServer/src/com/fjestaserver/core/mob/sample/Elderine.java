/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.mob.sample;

import com.fjestacore.common.map.MapDefault;
import com.fjestaserver.core.mob.DefaultMob;

/**
 * Elderine
 * @author FantaBlueMystery
 */
public class Elderine extends DefaultMob {

	/**
	 * Elderine
	 */
	public Elderine() {
		this.setMode(84);
		this.setMobId(3027);
		this.getPosition().setX(MapDefault.REGEN_X);
		this.getPosition().setY(MapDefault.REGEN_Y);
		this.getPosition().setRotation(10);
		this.setAnimationLevel(5);
		this.getBarsMax().setHp(400);
		this.getBars().setHp(200);
	}
}