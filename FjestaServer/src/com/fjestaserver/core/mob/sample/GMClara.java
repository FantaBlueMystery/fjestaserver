/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.mob.sample;

import com.fjestacore.common.chat.SomeoneChat;
import com.fjestacore.common.map.MapDefault;
import com.fjestacore.common.map.MapInfo;
import com.fjestacore.packet.act.NC_ACT_SOMEONECHAT_CMD;
import com.fjestaserver.core.mob.NpcPlayer;
import com.fjestaserver.zone.ClientZone;

/**
 * GMClara
 * GMClara test NPC char on the map
 * @author FantaBlueMystery
 */
public class GMClara extends NpcPlayer {

	/**
	 * GMClara
	 */
	public GMClara() {
		super(
			"00015B474D5D43616C61726100000000000000000000761900002311000003010"
			+ "E3A3F1F25CC31FFFFFFFFC931EC31CA31CB31FFFFFFFF87325F81FFFFFFFFFFF"
			+ "FFFFFFFFFFFFF7E7FF377FFFF0C00000000FFFFFFFFFF0000000000000000000"
			+ "0040000000000000000000000000000000000000000000000000000000000000"
			+ "0000000000000000000000000000000000000000000000000000400000000000"
			+ "0000000000000000000000000000000000000000000000000000000000000000"
			+ "0000000000000000000030000020000960000000000000000000000000000000"
			+ "000000000000000000000000000000000FFFF0A020001", false);

		MapInfo mapInfo = MapDefault.getMapInfo();

		this.getPosition().setX(mapInfo.getRegenX() - 30);
		this.getPosition().setY(mapInfo.getRegenY() - 30);
	}

	/**
	 * onNPCClick
	 * @param client
	 */
	@Override
	public void onNPCClick(ClientZone client) {
		client.writePacket(new NC_ACT_SOMEONECHAT_CMD(
			new SomeoneChat(
				this.getObjectHandle(),
				2,
				"Willkommen auf dem SimpleServer! Folgt uns auf den Discord!"
			)
		));
	}
}