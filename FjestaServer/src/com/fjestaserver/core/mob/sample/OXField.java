/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.mob.sample;

import com.fjestacore.common.chat.SomeoneChat;
import com.fjestacore.common.kq.KQTeamType;
import com.fjestacore.common.map.MapDefault;
import com.fjestacore.common.mob.MobBriefFlag;
import com.fjestacore.common.servermenu.ServerMenu;
import com.fjestacore.common.servermenu.ServerMenuEntry;
import com.fjestacore.packet.act.NC_ACT_SOMEONECHAT_CMD;
import com.fjestaserver.core.menu.Menu;
import com.fjestaserver.core.menu.MenuEntry;
import com.fjestaserver.core.mob.NpcMob;
import com.fjestaserver.core.net.Client;

/**
 * OXField
 * @author FantaBlueMystery
 */
public class OXField extends NpcMob {

	/**
	 * XOField
	 */
	public OXField() {
		this.setMode(2);
		this.setMobId(138);
		this.getPosition().setX(MapDefault.REGEN_X+30);
		this.getPosition().setY(MapDefault.REGEN_Y+30);
		this.getPosition().setRotation(20);

		this.getMobFlag().setFlagState(MobBriefFlag.GATE);
		this.getMobFlag().setGate2Where("OX_field");
		this.setKQTeamType(KQTeamType.KQTT_MAX);

		this._menu = new Menu("OX", 300);

		this._menu.addMenuEntry(new MenuEntry(0, "Yes", (ServerMenu sm, ServerMenuEntry sme, Client client) -> {
			client.writePacket(new NC_ACT_SOMEONECHAT_CMD(
				new SomeoneChat(
					sm.getObjectHandle(),
					2,
					"Du hast ja gedrückt!"
				)
			));
		}));


		this._menu.addMenuEntry(new MenuEntry(1, "No", (ServerMenu sm, ServerMenuEntry sme, Client client) -> {
			client.writePacket(new NC_ACT_SOMEONECHAT_CMD(
				new SomeoneChat(
					sm.getObjectHandle(),
					2,
					"Nein heißt nein!"
				)
			));
		}));
	}
}