/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.net;

import com.fjestacore.core.crypt.INetCrypt;
import com.fjestacore.core.crypt.NetCryptProvider;
import com.fjestacore.core.net.SocketType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDump;
import com.fjestacore.packet.PacketReader;
import com.fjestacore.packet.PacketUnReady;
import com.fjestacore.packet.PacketWriter;
import com.fjestacore.packet.misc.NC_MISC_SEED_ACK;
import com.fjestacore.socket.ClientBase;
import com.fjestacore.socket.ServerBase;
import com.fjestaserver.core.heartbeat.Heartbeat;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.handler.HandlerDictionary;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ServerClient
 * @author FantaBlueMystery
 */
abstract public class Client extends ClientBase {

	/**
	 * server
	 */
	protected ServerBase _server = null;

	/**
	 * session
	 */
	protected ISession _session = null;

	/**
	 * heatbeat
	 */
	protected Heartbeat _heatbeat = null;

	/**
	 * ServerClient
	 * @param server
	 * @param clientSocket
	 */
	public Client(ServerBase server, Socket clientSocket) {
		this._log		= Logger.getLogger(this.getClass().getName());
		this._server	= server;
		this._socket	= clientSocket;

		this._init();
	}

	/**
	 * init
	 */
	protected void _init() {
		Random rand = new Random();

		int offset = Math.abs(rand.nextInt(499));

		try {
			this._crypt = NetCryptProvider.getCrypt();
			this._crypt.setOffset(offset);
		}
		catch (Exception ex) {
			Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);

			this.close();

			return;
		}

		this._sendXorKey((short) offset);
		this._heatbeat = new Heartbeat();
	}

	/**
	 * log
	 * @param level
	 * @param msg
	 */
	public void log(Level level, String msg) {
		String sessionstr = "";

		if( this.getSession() != null ) {
			sessionstr = ":" + this.getSession().getId();
		}

		this.getLogger().log(level, this._getClassName() + "[{0}{1}] {2}",
			new Object[]{getInetAddress().toString(), sessionstr, msg});
	}

	/**
	 * log
	 * @param level
	 * @param msg
	 * @param param
	 */
	public void log(Level level, String msg, Object param) {
		this.log(level, msg, new Object[]{param});
	}

	/**
	 * log
	 * @param level
	 * @param msg
	 * @param params
	 */
	public void log(Level level, String msg, Object params[]) {
		String sessionstr = "";

		if( this.getSession() != null ) {
			sessionstr = ":" + this.getSession().getId();
		}

		this.getLogger().log(level, this._getClassName() + "[" +
			getInetAddress().toString() + sessionstr + "] " + msg, params);
	}

	/**
	 * getServer
	 * @return
	 */
	public ServerBase getServer() {
		return this._server;
	}

	/**
	 * getSession
	 * @return
	 */
	public ISession getSession() {
		return this._session;
	}

	/**
	 * setSession
	 * @param session
	 */
	public void setSession(ISession session) {
		this._session = session;
	}

	/**
	 * _sendXorKey
	 * @param offset
	 */
	protected void _sendXorKey(short offset) {
		if( (this._socket != null) && (this._socket.isConnected()) ) {
			NC_MISC_SEED_ACK sxk = new NC_MISC_SEED_ACK();
			sxk.getSeed().setOffset(offset);

			this._writePacket(sxk);
		}
	}

	/**
	 * _onSocketLoop
	 */
	@Override
	protected void _onSocketLoop() {
		this._heatbeat.performe(this);
	}

	/**
	 * _onPacket
	 * overwrite
	 * @param pac
	 */
	@Override
	protected void _onPacket(Packet pac) {
		try {
			HandlerDictionary.getInstance().call(this, pac);
		}
		catch( IllegalAccessException | IllegalArgumentException | InvocationTargetException ex ) {
			if( this._debug ) {
				this._log.log(Level.SEVERE, "_onPacket", ex);
			}
		}

		NcClient cmd = NcClient.valueOf(pac.getCommand());

		switch( cmd ) {
			case NC_MISC_HEARTBEAT_ACK:
				this._heatbeat.onBeat();
				break;
		}
	}

	/**
	 * _readPacket
	 * @param is
	 * @return
	 * @throws java.lang.InterruptedException
	 */
	@Override
	protected Packet _readPacket(InputStream is) throws InterruptedException {
		Packet pac = PacketReader.readPacket(is, this._crypt, SocketType.CLIENT, true);

		if( pac != null ) {
			pac.setType(SocketType.CLIENT);
		}

		return pac;
	}

	/**
	 * _readUnreadyPacket
	 * @param upac
	 * @param is
	 * @return
	 */
	@Override
	protected Packet _readUnreadyPacket(PacketUnReady upac, InputStream is) {
		Packet pac = PacketReader.readUnreadyPacket(upac, is, this._crypt, SocketType.CLIENT);

		return pac;
	}

	/**
	 * _getClassName
	 * @return
	 */
	@Override
	protected String _getClassName() {
		return this.getClass().getName().replace("com.fjestaserver.net.", "");
	}

	/**
	 * _writePacket
	 * @param packet
	 * @param crypt
	 * @return
	 */
	@Override
	protected boolean _writePacket(Packet packet, INetCrypt crypt) {
		try {
			packet.setType(SocketType.SERVER);

			byte[] buffer = PacketWriter.writePacket(packet, crypt);

			if( this._debug ) {
				NcServer cmd = NcServer.valueOf(packet.getCommand());

				switch( cmd ) {
					default:
						PacketDump dump = (PacketDump)packet.cast(PacketDump.class);

						this._log.info(dump.dumpStr(this._getClassName() + "-Write"));
				}
			}

			if( buffer != null ) {
				return this._writeRaw(buffer);
			}
			else {
				return false;
			}
		}
		catch( Exception ex ) {
			this._log.log(Level.SEVERE, "_writePacket", ex);
		}

		return false;
	}

	/**
	 * _writePacket
	 * @param pac
	 * @return
	 */
	@Override
	protected boolean _writePacket(Packet pac) {
		return this._writePacket(pac, null);
	}

	/**
	 * writePacket
	 * @param pac
	 * @return
	 */
	public boolean writePacket(Packet pac) {
		return this._writePacket(pac);
	}
}