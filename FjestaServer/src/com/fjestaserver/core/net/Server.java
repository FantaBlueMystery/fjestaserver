/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.net;

import com.fjestacore.packet.Packet;
import com.fjestacore.socket.ClientBase;
import com.fjestacore.socket.ServerBase;
import com.fjestacore.socket.config.SocketConfig;
import com.fjestaserver.core.session.ISession;

/**
 * Server
 * @author FantaBlueMystery
 */
abstract public class Server extends ServerBase {

	/**
	 * Server
	 */
	public Server() {
		super();
	}

	/**
	 * Server
	 * @param prop
	 */
	public Server(SocketConfig prop) {
		super(prop);
	}

	/**
	 * writePacketBroadcast
	 * @param pac
	 * @return
	 */
	public int writePacketBroadcast(Packet pac) {
		return this.writePacketBroadcast(pac, null);
	}

	/**
	 * writePacketBroadcast
	 * @param pac
	 * @param ignorClientSessionId
	 * @return
	 */
	public int writePacketBroadcast(Packet pac, String ignorClientSessionId) {
		int count = 0;

		for( ClientBase tclient: this._clients ) {
			Client c = (Client) tclient;

			ISession tSession = c.getSession();

			if( tSession != null ) {
				if( ignorClientSessionId != null ) {
					if( tSession.getId().compareTo(ignorClientSessionId) == 0 ) {
						continue;
					}
				}

				c.writePacket(pac);

				count++;
			}
		}

		return count;
	}
}