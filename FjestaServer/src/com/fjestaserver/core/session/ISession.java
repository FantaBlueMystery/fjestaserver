/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.session;

import com.fjestacore.client.ClientType;
import com.fjestacore.common.world.IWorld;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.menu.IMenu;

/**
 * ISession
 * @author FantaBlueMystery
 */
public interface ISession extends com.fjestacore.server.ISession {

	/**
	 * getId
	 * @return
	 */
	public String getId();

	/**
	 * getOtp
	 * @param createNew
	 * @return
	 */
	public String getOtp(boolean createNew);

	/**
	 * getOtp
	 * @return
	 */
	public String getOtp();

	/**
	 * getAccount
	 * @return
	 */
	public IAccount getAccount();

	/**
	 * setAccount
	 * @param account
	 */
	public void setAccount(IAccount account);

	/**
	 * getClientType
	 * @return
	 */
	public ClientType getClientType();

	/**
	 * setClientType
	 * @param type
	 */
	public void setClientType(ClientType type);

	/**
	 * getWorld
	 * @return
	 */
	public IWorld getWorld();

	/**
	 * setWorld
	 * @param world
	 */
	public void setWorld(IWorld world);

	/**
	 * getCharacter
	 * @return
	 */
	public ICharacterServer getCharacter();

	/**
	 * setCharacter
	 * @param character
	 */
	public void setCharacter(ICharacterServer character);

	/**
	 * getSelectedTarget
	 * @return
	 */
	public IMapObject getSelectedTarget();

	/**
	 * setSelectedTarget
	 * @param object
	 */
	public void setSelectedTarget(IMapObject object);

	/**
	 * getUsedMenu
	 * @return
	 */
	public IMenu getUsedMenu();

	/**
	 * setUsedMenu
	 * @param menu
	 */
	public void setUsedMenu(IMenu menu);
}