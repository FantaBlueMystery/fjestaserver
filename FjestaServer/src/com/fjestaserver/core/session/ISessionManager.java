/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.session;

/**
 * ISessionManager
 * @author FantaBlueMystery
 */
public interface ISessionManager {

	/**
	 * openSession
	 * open a new session
	 * @return
	 */
	public ISession openSession();

	/**
	 * closeSession
	 * @param session
	 */
	public void closeSession(ISession session);

	/**
	 * getSession
	 * @param id
	 * @return
	 */
	public ISession getSession(String id);

	/**
	 * getSession
	 * @param id
	 * @return
	 */
	public ISession getSession(byte[] id);

	/**
	 * getSession
	 * @param worldHandle
	 * @return
	 */
	public ISession getSession(long worldHandle);

	/**
	 * getSessionByOtp
	 * @param otp
	 * @return
	 */
	public ISession getSessionByOtp(String otp);
}