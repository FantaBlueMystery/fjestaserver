/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.session;

import com.fjestacore.client.ClientType;
import com.fjestacore.common.name.Name8;
import com.fjestacore.common.world.IWorld;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.menu.IMenu;

/**
 * Session
 * @author FantaBlueMystery
 */
public class Session implements ISession {

	/**
	 * id
	 */
	protected String _id = "";

	/**
	 * otp
	 */
	protected String _otp = null;

	/**
	 * account
	 */
	protected IAccount _account = null;

	/**
	 * client type
	 */
	protected ClientType _clientType = ClientType.NULL;

	/**
	 * world
	 */
	protected IWorld _world = null;

	/**
	 * world (client) handle
	 */
	protected int _worldHandle = 0;

	/**
	 * object handle
	 */
	protected long _objectHandle = 0;

	/**
	 * character
	 */
	protected ICharacterServer _character = null;

	/**
	 * selected object
	 */
	protected IMapObject _selectedObject = null;

	/**
	 * used menu
	 */
	protected IMenu _usedMenu = null;

	/**
	 * Session
	 * @param id
	 */
	public Session(String id) {
		this._id = id;
	}

	/**
	 * getId
	 * @return
	 */
	@Override
	public String getId() {
		return this._id;
	}

	/**
	 * getOtp
	 * @param createNew
	 * @return
	 */
	public String getOtp(boolean createNew) {
		if( createNew ) {
			this._otp = SessionOTPGenerator.getOtp(Name8.SIZE);
		}

		return this._otp;
	}

	/**
	 * getOtp
	 * @return
	 */
	@Override
	public String getOtp() {
		return this._otp;
	}

	/**
	 * getIdBytes
	 * @return
	 */
	@Override
	public byte[] getIdBytes() {
		return this._id.getBytes();
	}

	/**
	 * getAccount
	 * @return
	 */
	@Override
	public IAccount getAccount() {
		return this._account;
	}

	/**
	 * setAccount
	 * @param account
	 */
	@Override
	public void setAccount(IAccount account) {
		this._account = account;
	}

	/**
	 * getClientType
	 * @return
	 */
	@Override
	public ClientType getClientType() {
		return this._clientType;
	}

	/**
	 * setClientType
	 * @param type
	 */
	@Override
	public void setClientType(ClientType type) {
		this._clientType = type;
	}

	/**
	 * getWorld
	 * @return
	 */
	@Override
	public IWorld getWorld() {
		return this._world;
	}

	/**
	 * setWorld
	 * @param world
	 */
	@Override
	public void setWorld(IWorld world) {
		this._world = world;
	}

	/**
	 * getServerHandle
	 * @return
	 */
	@Override
	public int getWorldHandle() {
		return this._worldHandle;
	}

	/**
	 * setWorldHandle
	 * @param handle
	 */
	@Override
	public void setWorldHandle(int handle) {
		this._worldHandle = handle;
	}

	/**
	 * getCharacter
	 * @return
	 */
	@Override
	public ICharacterServer getCharacter() {
		return this._character;
	}

	/**
	 * setCharacter
	 * @param character
	 */
	@Override
	public void setCharacter(ICharacterServer character) {
		this._character = character;
	}

	/**
	 * getSelectedTarget
	 * @return
	 */
	@Override
	public IMapObject getSelectedTarget() {
		return this._selectedObject;
	}

	/**
	 * setSelectedTarget
	 * @param object
	 */
	@Override
	public void setSelectedTarget(IMapObject object) {
		this._selectedObject = object;
	}

	/**
	 * getUsedMenu
	 * @return
	 */
	@Override
	public IMenu getUsedMenu() {
		return this._usedMenu;
	}

	/**
	 * setUsedMenu
	 * @param menu
	 */
	@Override
	public void setUsedMenu(IMenu menu) {
		this._usedMenu = menu;
	}
}