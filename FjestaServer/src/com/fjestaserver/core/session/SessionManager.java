/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.session;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * SessionManager
 * @desciption this object is basic object and workt in memory, override and save all in database
 * @author FantaBlueMystery
 */
public class SessionManager implements ISessionManager {

	/**
	 * session manager
	 */
	static protected ISessionManager _instance;

	/**
	 * getInstance
	 * return instance of session manager
	 * @return
	 */
	static public SessionManager getInstance() {
		if( SessionManager._instance == null ) {
			SessionManager._instance = new SessionManager();
		}

		return (SessionManager) SessionManager._instance;
	}

	/**
	 * _createFreeSessionToken
	 * @return
	 */
	protected String _createFreeSessionToken() {
		UUID partOne = UUID.randomUUID();
		UUID partTwo = UUID.randomUUID();

		String sessionFull = partOne.toString() + partTwo.toString();

		sessionFull = sessionFull.replaceAll("-", "");

		return sessionFull.substring(0, 45);
	}

	/**
	 * sessions
	 */
	private List<ISession> _sessions = null;

	/**
	 * SessionManager
	 */
	public SessionManager() {
		this._sessions = new ArrayList<>();
	}

	/**
	 * openSession
	 * @return
	 */
	@Override
	public ISession openSession() {
		if( this._sessions == null ) {
			return null;
		}

		Session nsession = new Session(this._createFreeSessionToken());

		this._sessions.add(nsession);

		return nsession;
	}

	/**
	 * closeSession
	 * @param session
	 */
	@Override
	public void closeSession(ISession session) {
		if( this._sessions == null ) {
			return;
		}

		this._sessions.remove(session);
	}

	/**
	 * getSession
	 * @param id
	 * @return
	 */
	@Override
	public ISession getSession(String id) {
		if( this._sessions == null ) {
			return null;
		}

		for( ISession asession: this._sessions ) {
			if( asession.getId().compareTo(id) == 0 ) {
				return asession;
			}
		}

		return null;
	}

	/**
	 * getSession
	 * @param id
	 * @return
	 */
	@Override
	public ISession getSession(byte[] id) {
		if( this._sessions == null ) {
			return null;
		}

		for( ISession asession: this._sessions ) {
			String sessionStr = new String(id);

			if( sessionStr.trim().compareTo(asession.getId()) == 0) {
				return asession;
			}
		}

		return null;
	}

	/**
	 * getSession
	 * @param worldHandle
	 * @return
	 */
	@Override
	public ISession getSession(long worldHandle) {
		if( this._sessions == null ) {
			return null;
		}

		for( ISession asession: this._sessions ) {
			if( asession.getWorldHandle() == worldHandle ) {
				return asession;
			}
		}

		return null;
	}

	/**
	 * getSessionByOtp
	 * @param otp
	 * @return
	 */
	@Override
	public ISession getSessionByOtp(String otp) {
		if( this._sessions == null ) {
			return null;
		}

		for( ISession asession: this._sessions ) {
			String sotp = asession.getOtp();

			if( sotp != null ) {
				if( otp.compareTo(sotp) == 0 ) {
					return asession;
				}
			}
		}

		return null;
	}
}
