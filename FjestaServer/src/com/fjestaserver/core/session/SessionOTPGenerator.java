/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.session;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * SessionOTPGenerator
 * @author FantaBlueMystery
 */
public class SessionOTPGenerator {

	/**
	 * getOtp
	 * @param size
	 * @return
	 */
	static public String getOtp(int size) {
        StringBuilder generatedToken = new StringBuilder();

        try {
            SecureRandom number = SecureRandom.getInstance("SHA1PRNG");

            for( int i= 0; i<size; i++ ) {
                generatedToken.append(number.nextInt(9));
            }
        }
		catch( NoSuchAlgorithmException e ) {
			e.printStackTrace();
        }

        return generatedToken.toString();
	}
}