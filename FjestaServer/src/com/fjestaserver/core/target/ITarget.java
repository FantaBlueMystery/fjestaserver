/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.target;

import com.fjestacore.common.bat.TargetInfo;

/**
 * ITarget
 * @author FantaBlueMystery
 */
public interface ITarget {

	/**
	 * getTargetInfo
	 * @return
	 */
	public TargetInfo getTargetInfo();
}