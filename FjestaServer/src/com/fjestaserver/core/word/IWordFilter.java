/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.word;

/**
 * IWordFilter
 * @author FantaBlueMystery
 */
public interface IWordFilter {

	/**
	 * getBadWord
	 * @return
	 */
	public String getBadWord();

	/**
	 * setBadWord
	 * @param word
	 */
	public void setBadWord(String word);

	/**
	 * getReplacement
	 * @return
	 */
	public String getReplacement();

	/**
	 * setReplacement
	 * @param replacement
	 */
	public void setReplacement(String replacement);
}