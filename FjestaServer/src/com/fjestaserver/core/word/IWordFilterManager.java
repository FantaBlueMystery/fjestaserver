/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.word;

/**
 * IWordFilterManager
 * @author FantaBlueMystery
 */
public interface IWordFilterManager {

	/**
	 * addWordFilter
	 * @param filter
	 */
	public void addWordFilter(IWordFilter filter);

	/**
	 * isFoundBadWord
	 * @param str
	 * @return
	 */
	public boolean isFoundBadWord(String str);

	/**
	 * replaceBadWords
	 * @param str
	 * @return
	 */
	public String replaceBadWords(String str);
}