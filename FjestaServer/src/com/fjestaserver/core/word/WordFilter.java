/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.word;

/**
 * WordFilter
 * @author FantaBlueMystery
 */
public class WordFilter implements IWordFilter {

	/**
	 * bad word
	 */
	protected String _badWord = "";

	/**
	 * replacement
	 */
	protected String _replacement = "";

	/**
	 * WordFilter
	 * @param badWord
	 * @param replacement
	 */
	public WordFilter(String badWord, String replacement) {
		this._badWord = badWord;
		this._replacement = replacement;
	}

	/**
	 * getBadWord
	 * @return
	 */
	@Override
	public String getBadWord() {
		return this._badWord;
	}

	/**
	 * setBadWord
	 * @param word
	 */
	@Override
	public void setBadWord(String word) {
		this._badWord = word;
	}

	/**
	 * getReplacement
	 * @return
	 */
	@Override
	public String getReplacement() {
		return this._replacement;
	}

	/**
	 * setReplacement
	 * @param replacement
	 */
	@Override
	public void setReplacement(String replacement) {
		this._replacement = replacement;
	}
}