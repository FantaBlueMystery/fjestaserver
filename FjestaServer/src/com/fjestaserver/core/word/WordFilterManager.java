/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.word;

import java.util.ArrayList;

/**
 * WordFilterManager
 * @author FantaBlueMystery
 */
public class WordFilterManager implements IWordFilterManager {

	/**
	 * wordfilter manager
	 */
	static protected IWordFilterManager _instance;

	/**
	 * getInstance
	 * return instance of wordfilter manager
	 * @return
	 */
	static public WordFilterManager getInstance() {
		if( WordFilterManager._instance == null ) {
			WordFilterManager._instance = new WordFilterManager();
		}

		return (WordFilterManager) WordFilterManager._instance;
	}

	/**
	 * filters
	 */
	protected ArrayList<IWordFilter> _filters = new ArrayList<>();

	/**
	 * WordFilterManager
	 */
	public WordFilterManager() {
		// #default Filter#
		this.addWordFilter(new WordFilter("sex", "***"));
		this.addWordFilter(new WordFilter("dollar", "*real money not allowed*"));
		this.addWordFilter(new WordFilter("euro", "*real money not allowed*"));
	}

	/**
	 * addWordFilter
	 * @param filter
	 */
	@Override
	public void addWordFilter(IWordFilter filter) {
		this._filters.add(filter);
	}

	/**
	 * isFoundBadWord
	 * @param str
	 * @return
	 */
	@Override
	public boolean isFoundBadWord(String str) {
		for( IWordFilter twf: this._filters ) {
			if( str.contains(twf.getBadWord()) ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * replaceBadWords
	 * @param str
	 * @return
	 */
	@Override
	public String replaceBadWords(String str) {
		String tmp = str;

		for( IWordFilter twf: this._filters ) {
			tmp = tmp.replaceAll(twf.getBadWord(), twf.getReplacement());
		}

		return tmp;
	}
}
