/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.world;

import com.fjestacore.common.world.IWorld;
import com.fjestacore.common.world.IWorldList;

/**
 * IWorldManager
 * @author FantaBlueMystery
 */
public interface IWorldManager {

	/**
	 * getWorld
	 * @param id
	 * @return
	 */
	public IWorld getWorld(int id);

	/**
	 * getWorldList
	 * @return
	 */
	public IWorldList getWorldList();

	/**
	 * update
	 * @param world
	 */
	public void update(IWorld world);

	/**
	 * count
	 * @return
	 */
	public int count();
}