/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.core.world;

import com.fjestacore.common.world.IWorld;
import com.fjestacore.common.world.IWorldList;
import com.fjestacore.common.world.WorldList;
import java.util.Iterator;

/**
 * WorldManager
 * @author FantaBlueMystery
 */
public class WorldManager implements IWorldManager {

	/**
	 * WorldManager instance
	 */
	static protected WorldManager _instance;

	/**
	 * getInstance
	 * return instance of world manager
	 * @return
	 */
	static public WorldManager getInstance() {
		if( WorldManager._instance == null ) {
			WorldManager._instance = new WorldManager();
		}

		return WorldManager._instance;
	}

	/**
	 * worldlist
	 */
	protected WorldList _wl;

	/**
	 * WorldManager
	 */
	public WorldManager() {
		this._wl = new WorldList();
	}

	/**
	 * getWorldList
	 * @return
	 */
	@Override
	public IWorldList getWorldList() {
		if( this._wl == null ) {
			return null;
		}

		return this._wl;
	}

	/**
	 * getWorld
	 * @param id
	 * @return
	 */
	@Override
	public IWorld getWorld(int id) {
		if( this._wl == null ) {
			return null;
		}

		for( IWorld _world : this._wl.getList() ) {
			if( _world.getId() == id ) {
				return _world;
			}
		}

		return null;
	}

	/**
	 * update
	 * @param world
	 */
	@Override
	public void update(IWorld world) {
		if( this._wl == null ) {
			return;
		}

		Iterator itr = this._wl.getList().iterator();

		while( itr.hasNext() ) {
			IWorld _world = (IWorld) itr.next();

			if( _world.getId() == world.getId() ) {
				itr.remove();

				this._wl.add(world);
				break;
			}
		}
	}

	/**
	 * count
	 * @return
	 */
	@Override
	public int count() {
		if( this._wl == null ) {
			return 0;
		}

		return this._wl.getList().size();
	}
}