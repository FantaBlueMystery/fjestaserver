/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.zone;

/**
 * IZone
 * @author FantaBlueMystery
 */
public interface IZone {

	/**
	 * getId
	 * @return
	 */
	public int getId();

	/**
	 * getIndexName
	 * @return
	 */
	public String getIndexName();

	/**
	 * getServerIP
	 * @return
	 */
	public String getServerIP();

	/**
	 * getServerPort
	 * @return
	 */
	public int getServerPort();
}