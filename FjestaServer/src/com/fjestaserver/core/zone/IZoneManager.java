/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.zone;

/**
 * IZoneManager
 * @author FantaBlueMystery
 */
public interface IZoneManager {

	/**
	 * getZone
	 * @param id
	 * @return
	 */
	public IZone getZone(int id);

	/**
	 * getZone
	 * @param indexName
	 * @return
	 */
	public IZone getZone(String indexName);
}