/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.zone;

/**
 * Zone
 * @author FantaBlueMystery
 */
public class Zone implements IZone {

	/**
	 * id
	 */
	protected int _id = 0;

	/**
	 * index name
	 */
	protected String _indexName = "";

	/**
	 * ip
	 */
	protected String _serverIp = "";

	/**
	 * port
	 */
	protected int _serverPort = 0;

	/**
	 * getId
	 * @return
	 */
	@Override
	public int getId() {
		return this._id;
	}

	/**
	 * getIndexName
	 * @return
	 */
	@Override
	public String getIndexName() {
		return this._indexName;
	}

	/**
	 * getServerIP
	 * @return
	 */
	@Override
	public String getServerIP() {
		return this._serverIp;
	}

	/**
	 * getServerPort
	 * @return
	 */
	@Override
	public int getServerPort() {
		return this._serverPort;
	}
}