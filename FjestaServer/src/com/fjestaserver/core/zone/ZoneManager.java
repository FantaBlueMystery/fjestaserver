/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.core.zone;

import java.util.ArrayList;

/**
 * MapManager
 * @author FantaBlueMystery
 */
public class ZoneManager implements IZoneManager {

	/**
	 * zone manager
	 */
	static protected IZoneManager _instance;

	/**
	 * getInstance
	 * return instance of zone manager
	 * @return
	 */
	static public IZoneManager getInstance() {
		if( ZoneManager._instance == null ) {
			ZoneManager._instance = new ZoneManager();
		}

		return ZoneManager._instance;
	}

	/**
	 * zones
	 */
	protected ArrayList<IZone> _zones = new ArrayList<>();

	/**
	 * addZone
	 * @param azone
	 */
	public void addZone(IZone azone) {
		this._zones.add(azone);
	}

	/**
	 * getZone
	 * @param id
	 * @return
	 */
	@Override
	public IZone getZone(int id) {
		for( IZone _zone : this._zones ) {
			if( _zone.getId() == id ) {
				return _zone;
			}
		}

		return null;
	}

	/**
	 * getZone
	 * @param indexName
	 * @return
	 */
	@Override
	public IZone getZone(String indexName) {
		for( IZone _zone : this._zones ) {
			if( _zone.getIndexName().equals(indexName) ) {
				return _zone;
			}
		}

		return null;
	}
}