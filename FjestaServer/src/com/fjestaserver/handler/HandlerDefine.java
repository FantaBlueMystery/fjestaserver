/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * HandlerDefine
 * @author FantaBlueMystery
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface HandlerDefine {

	/**
	 * type
	 * @return
	 */
	ServerType type();

	/**
	 * command of packet
	 * @return
	 */
	NcClient cmd();
}