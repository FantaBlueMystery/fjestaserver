/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.packet.Packet;
import com.fjestaserver.core.net.Client;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * HandlerDictionary
 * @author FantaBlueMystery
 */
public class HandlerDictionary {

	/**
	 * SingletonHolder
	 */
	static private final class SingletonHolder {
		static private final HandlerDictionary INSTANCE = new HandlerDictionary();
	}

	/**
	 * getInstance
	 * return instance of HandlerDictionary
	 * @return
	 */
	static public HandlerDictionary getInstance() {
		return HandlerDictionary.SingletonHolder.INSTANCE;
	}

	/**
	 * handles
	 */
	private Map<ServerType, HashMap<Integer, Method>> _handles = new EnumMap<>(ServerType.class);

	/**
	 * addHandler
	 * @param classHandler
	 */
	public void addHandler(Class classHandler) {
		Method[] methods = classHandler.getMethods();

		for( Method m : methods ) {
			if( m.isAnnotationPresent( HandlerDefine.class ) ) {
				HandlerDefine a = m.getAnnotation( HandlerDefine.class );

				HashMap<Integer, Method> cmds;

				if( this._handles.containsKey(a.type()) ) {
				}
				else {
					this._handles.put(a.type(), new HashMap<>());
				}

				cmds = this._handles.get(a.type());

				cmds.put(a.cmd().getValue(), m);
			}
		}
	}

	/**
	 * call
	 * @param client
	 * @param packet
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public void call(Client client, Packet packet) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		HashMap<Integer, Method> cmds = this._handles.get(client.getServer().getServerType());

		if( cmds != null ) {
			Method m = cmds.get(packet.getCommand());

			if( m != null ) {
				m.invoke(null, client, packet);
			}
		}
	}
}