/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.login;

import com.fjestacore.socket.ServerBase;
import com.fjestaserver.core.net.Client;
import java.net.Socket;

/**
 * ClientLogin
 * @author FantaBlueMystery
 */
public class ClientLogin extends Client {

	/**
	 * ClientLogin
	 * @param server
	 * @param clientSocket
	 */
	public ClientLogin(ServerBase server, Socket clientSocket) {
		super(server, clientSocket);
	}

	/**
	 * _getClassName
	 * @return
	 */
	@Override
	protected String _getClassName() {
		return this.getClass().getName().replace("com.fjestaserver.login.", "");
	}

	/**
	 * getServer
	 * @return
	 */
	@Override
	public ServerLogin getServer() {
		return (ServerLogin) this._server;
	}
}