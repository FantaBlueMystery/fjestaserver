/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.login;

import com.fjestacore.core.net.ServerType;
import com.fjestaserver.handler.HandlerDictionary;
import com.fjestaserver.login.handler.H03User;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.core.net.Server;
import com.fjestaserver.login.config.LoginConfig;
import java.net.Socket;

/**
 * ServerLogin
 * @author FantaBlueMystery
 */
public class ServerLogin extends Server {

	/**
	 * ServerLogin
	 * @param prop
	 */
	public ServerLogin(LoginConfig prop) {
		super(prop);

		this._serverType = ServerType.LOGIN;

		HandlerDictionary.getInstance().addHandler(H03User.class);
	}

	/**
	 * getProperties
	 * @return
	 */
	@Override
	public LoginConfig getProperties() {
		return (LoginConfig) this._properties;
	}

	/**
	 * castSocket
	 * @param cs
	 * @return
	 */
	@Override
	protected Client _castSocket(Socket cs) {
		return new ClientLogin(this, cs);
	}
}