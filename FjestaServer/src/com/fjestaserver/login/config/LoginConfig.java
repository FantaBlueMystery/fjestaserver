/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.login.config;

import com.fjestacore.socket.config.SocketConfig;

/**
 * LoginConfig
 * @author FantaBlueMystery
 */
public class LoginConfig extends SocketConfig {

	/**
	 * Properties key
	 */
	static public String CHECK_CLIENT_VERSION		= "check_client_version";

	/**
	 * isCheckClientVersion
	 * @return
	 */
	public boolean isCheckClientVersion() {
		String ccv = this.getProperty(LoginConfig.CHECK_CLIENT_VERSION, "1");

		return ccv.startsWith("1");
	}
}