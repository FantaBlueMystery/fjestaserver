/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.login.handler;

import com.fjestacore.client.ClientType;
import com.fjestacore.common.login.ILogin;
import com.fjestacore.common.user.UserClientVersion;
import com.fjestacore.common.user.XtrapReq;
import com.fjestacore.common.world.IWorld;
import com.fjestacore.common.world.WorldConnect;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.PacketDump;
import com.fjestacore.packet.user.NC_USER_CLIENT_RIGHTVERSION_CHECK_ACK;
import com.fjestacore.packet.user.NC_USER_CLIENT_VERSION_CHECK_REQ;
import com.fjestacore.packet.user.NC_USER_CLIENT_WRONGVERSION_CHECK_ACK;
import com.fjestacore.packet.user.NC_USER_GER_LOGIN_REQ;
import com.fjestacore.packet.user.NC_USER_LOGINFAIL_ACK;
import com.fjestacore.packet.user.NC_USER_LOGIN_ACK;
import com.fjestacore.packet.user.NC_USER_LOGIN_WITH_OTP_REQ;
import com.fjestacore.packet.user.NC_USER_NORMALLOGOUT_CMD;
import com.fjestacore.packet.user.NC_USER_US_LOGIN_REQ;
import com.fjestacore.packet.user.NC_USER_WORLDSELECT_ACK;
import com.fjestacore.packet.user.NC_USER_WORLDSELECT_REQ;
import com.fjestacore.packet.user.NC_USER_WORLD_STATUS_REQ;
import com.fjestacore.packet.user.NC_USER_XTRAP_ACK;
import com.fjestacore.packet.user.NC_USER_XTRAP_REQ;
import com.fjestacore.server.ServerError;
import com.fjestaserver.core.account.AccountManager;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.account.IAccountManager;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.core.session.ISessionManager;
import com.fjestaserver.core.session.SessionManager;
import com.fjestaserver.core.world.IWorldManager;
import com.fjestaserver.core.world.WorldManager;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.login.ClientLogin;
import com.fjestaserver.login.ServerLogin;
import java.util.logging.Level;

/**
 * H03User
 * @author FantaBlueMystery
 */
public class H03User {

	/**
	 * NC_USER_CLIENT_VERSION_CHECK_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.LOGIN, cmd = NcClient.NC_USER_CLIENT_VERSION_CHECK_REQ)
	static public void NC_USER_CLIENT_VERSION_CHECK_REQ(ClientLogin client, NC_USER_CLIENT_VERSION_CHECK_REQ packet) {
		ServerLogin sl	= client.getServer();
		String cIp		= client.getInetAddress().toString();

		if( sl.getProperties().isCheckClientVersion() ) {
			client.log(Level.INFO, "Client UserVersion check enable.");

			UserClientVersion ucv = packet.getUserClientVersion();

			if( ucv.compareHash(new UserClientVersion()) == 0 ) {
				client.log(Level.INFO, "Client UserVersion allowed by: {0}", cIp);

				client.writePacket(new NC_USER_CLIENT_RIGHTVERSION_CHECK_ACK());
			}
			else {
				PacketDump pd = (PacketDump)packet.cast(PacketDump.class);

				client.log(Level.INFO, "Client UserVersion to old: {0}",
					new Object[]{pd.dumpStr("Login-Server")});

				client.writePacket(new NC_USER_CLIENT_WRONGVERSION_CHECK_ACK());
			}
		}
		else {
			client.log(Level.INFO, "Client UserVersion check disable.");
			client.log(Level.INFO, "Client UserVersion allowed by: {0}", cIp);

			client.writePacket(new NC_USER_CLIENT_RIGHTVERSION_CHECK_ACK());
		}
	}

	/**
	 * NC_USER_XTRAP_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.LOGIN, cmd = NcClient.NC_USER_XTRAP_REQ)
	static public void NC_USER_XTRAP_REQ(ClientLogin client, NC_USER_XTRAP_REQ packet) {
		if( client.getSession() != null ) {
			XtrapReq fh = new XtrapReq();
			NC_USER_XTRAP_ACK fca = new NC_USER_XTRAP_ACK();

			fca.getXtrapAck().setAllow(false);

			if( fh.compare(packet.getXtrapReq()) ) {
				fca.getXtrapAck().setAllow(true);
			}

			client.writePacket(fca);
		}
	}

	/**
	 * NC_USER_GER_LOGIN_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.LOGIN, cmd = NcClient.NC_USER_GER_LOGIN_REQ)
	static public void NC_USER_GER_LOGIN_REQ(ClientLogin client, NC_USER_GER_LOGIN_REQ packet) {
		IAccountManager am = AccountManager.getInstance();

		if( am == null ) {
			client.log(Level.WARNING, "AccountManager is empty instance.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.UNKNOWN_ERROR));
			return;
		}

		ILogin login = packet.getLogin();
		IAccount account = am.getAccount(login.getUsername());

		if( account == null ) {
			client.log(Level.INFO, "Client Account not found by Username: {0}", login.getUsername());
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.CHECK_ID_OR_PASS));
			return;
		}

		if( !am.comparePassword(login.getPassword(), account.getPassword()) ) {
			client.log(Level.INFO, "Client Account password does not match by Username: {0}", login.getUsername());
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.CHECK_ID_OR_PASS));
			return;
		}

		ISessionManager sm = SessionManager.getInstance();

		if( sm == null ) {
			client.log(Level.WARNING, "SessionManager is empty instance.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.UNKNOWN_ERROR));
			return;
		}

		ISession session = sm.openSession();

		client.log(Level.INFO, "New Session: {0}", session.getId());
		client.log(Level.INFO, "Client-Version: {0}", ClientType.GER.getValue());

		session.setAccount(account);
		session.setClientType(ClientType.GER);

		client.setSession(session);

		// send world list
		IWorldManager wm = WorldManager.getInstance();

		if( wm != null ) {
			if( wm.getWorldList().count() == 0 ) {
				client.log(Level.WARNING, "World list is empty.");
				client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.LOGIN_FAILED));
				return;
			}

			client.writePacket(new NC_USER_LOGIN_ACK(wm.getWorldList()));
			return;
		}

		client.log(Level.WARNING, "WorldManager is empty instance.");
		client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.LOGIN_FAILED));
	}

	/**
	 * NC_USER_US_LOGIN_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.LOGIN, cmd = NcClient.NC_USER_US_LOGIN_REQ)
	static public void NC_USER_US_LOGIN_REQ(ClientLogin client, NC_USER_US_LOGIN_REQ packet) {
		IAccountManager am = AccountManager.getInstance();

		if( am == null ) {
			client.log(Level.WARNING, "AccountManager is empty instance.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.UNKNOWN_ERROR));
			return;
		}

		ILogin login = packet.getLogin();
		IAccount account = am.getAccount(login.getUsername());

		//TODO
	}

	/**
	 * NC_USER_WORLD_STATUS_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.LOGIN, cmd = NcClient.NC_USER_WORLD_STATUS_REQ)
	static public void NC_USER_WORLD_STATUS_REQ(ClientLogin client, NC_USER_WORLD_STATUS_REQ packet) {
		ISession session = client.getSession();

		if( session == null ) {
			client.log(Level.WARNING, "Session is empty.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.AUTH_TIMED_OUT));
			return;
		}

		IAccount account = session.getAccount();

		if( account == null ) {
			client.log(Level.WARNING, "Account is empty.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.AUTH_TIMED_OUT));
			return;
		}

		// send world list
		IWorldManager wm = WorldManager.getInstance();

		if( wm == null ) {
			client.log(Level.WARNING, "WorldManager is empty instance.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.AUTH_TIMED_OUT));
			return;
		}

		client.writePacket(new NC_USER_LOGIN_ACK(wm.getWorldList()));
	}

	/**
	 * NC_USER_WORLDSELECT_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.LOGIN, cmd = NcClient.NC_USER_WORLDSELECT_REQ)
	static public void NC_USER_WORLDSELECT_REQ(ClientLogin client, NC_USER_WORLDSELECT_REQ packet) {
		ISession session = client.getSession();

		if( session == null ) {
			client.log(Level.WARNING, "Session is empty.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.UNKNOWN_ERROR));
			return;
		}

		IAccount account = session.getAccount();

		if( account == null ) {
			client.log(Level.WARNING, "Account is empty.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.AUTH_TIMED_OUT));
			return;
		}

		IWorldManager wm = WorldManager.getInstance();

		if( wm == null ) {
			client.log(Level.WARNING, "WorldManager is empty instance.");
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.AUTH_TIMED_OUT));
			return;
		}

		IWorld world = wm.getWorld(packet.getSelect().getWorldId());

		if( world == null ) {
			client.log(Level.WARNING, "World not found: {0}", packet.getSelect().getWorldId());
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.WRONG_REGION));
			return;
		}

		session.setWorld(world);

		// send connection to client
		client.writePacket(new NC_USER_WORLDSELECT_ACK(new WorldConnect(world, session)));
	}

	/**
	 * NC_USER_NORMALLOGOUT_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.LOGIN, cmd = NcClient.NC_USER_NORMALLOGOUT_CMD)
	static public void NC_USER_NORMALLOGOUT_CMD(ClientLogin client, NC_USER_NORMALLOGOUT_CMD packet) {
		ServerLogin ssl = client.getServer();

		ISession session = client.getSession();

		if( session != null ) {
			client.log(Level.INFO, "normal logout");

			ISessionManager sm = SessionManager.getInstance();
			sm.closeSession(session);
		}

		// remove client
		ssl.removeClient(client);
	}

	/**
	 * NC_USER_LOGIN_WITH_OTP_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.LOGIN, cmd = NcClient.NC_USER_LOGIN_WITH_OTP_REQ)
	static public void NC_USER_LOGIN_WITH_OTP_REQ(ClientLogin client, NC_USER_LOGIN_WITH_OTP_REQ packet) {
		String otp = packet.getUserLoginWithOtp().getOtp();

		ISessionManager sm = SessionManager.getInstance();
		ISession session = sm.getSessionByOtp(otp);

		if( session == null ) {
			client.log(Level.WARNING, "Session is empty by otp: " + otp);
			client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.AUTH_TIMED_OUT));
			return;
		}

		client.setSession(session);

		// send world list
		IWorldManager wm = WorldManager.getInstance();

		if( wm != null ) {
			if( wm.getWorldList().count() == 0 ) {
				client.log(Level.WARNING, "World list is empty.");
				client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.LOGIN_FAILED));
				return;
			}

			client.writePacket(new NC_USER_LOGIN_ACK(wm.getWorldList()));
			return;
		}

		client.log(Level.WARNING, "WorldManager is empty instance.");
		client.writePacket(new NC_USER_LOGINFAIL_ACK(ServerError.LOGIN_FAILED));
	}
}
