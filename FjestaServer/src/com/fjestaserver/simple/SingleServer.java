/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.simple;

import com.fjestaserver.core.guild.Guild;
import com.fjestaserver.core.guild.GuildManager;
import com.fjestaserver.login.ServerLogin;
import com.fjestaserver.simple.accounts.SimpleAccountManager;
import com.fjestaserver.simple.characters.SimpleCharacterManager;
import com.fjestaserver.simple.config.SimpleConfig;
import com.fjestaserver.simple.config.SimpleConfigLoader;
import com.fjestaserver.simple.world.SimpleWorldManager;
import com.fjestaserver.simple.zone.SimpleZoneManager;
import com.fjestaserver.world.ServerWorld;
import com.fjestaserver.zone.ServerZone;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SingleServer
 * Simple all in one server definition for DEV & Tests
 * @author FantaBlueMystery
 */
public class SingleServer {

	/**
	 * properties for servers
	 */
	protected SimpleConfig _properties = null;

	/**
	 * a server login
	 */
	protected ServerLogin _serverLogin = null;

	/**
	 * a server world
	 */
	protected ServerWorld _serverWorld = null;

	/**
	 * a server zone
	 */
	protected ServerZone _serverZone = null;

	/**
	 * SingleServer
	 * @param properties
	 */
	public SingleServer(SimpleConfig properties) {
		this._properties = properties;
	}

	/**
	 * start
	 */
	public void start() {
		this._serverLogin	= new ServerLogin(this._properties.getLoginConfig());
		this._serverWorld	= new ServerWorld(this._properties.getWorldConfig());
		this._serverZone	= new ServerZone(this._properties.getZoneConfig());

		try {
			this._serverLogin.start();
			this._serverWorld.start();
			this._serverZone.start();
		}
		catch( IOException ex ) {
			Logger.getLogger(SingleServer.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(1);
		}
	}

	/**
	 * main
	 * @param args
	 */
	public static void main(String[] args) {
		// Infos
		// ---------------------------------------------------------------------

		System.out.println("FjestaServer & FjestaCore API - Test SingleServer with simple-class");
		System.out.println("Use this Version only by development for your own server!");
		System.out.println("@Author FantaBlueMystery");
		System.out.println("");

		// Load Config
		// ---------------------------------------------------------------------

		String configPath = SimpleConfigLoader.PROPERTY_FILE;

		if( args.length >= 1 ) {
			configPath = args[0];
		}

		try {
			SimpleConfigLoader.setInstance(new SimpleConfigLoader(configPath));
		}
		catch( IOException ex ) {
			Logger.getLogger(SingleServer.class.getName()).log(Level.SEVERE, "Can not load your config file: {0}", configPath);
			System.exit(1);
		}

		SimpleConfig config = null;

		try {
			config = (SimpleConfig) SimpleConfigLoader.getInstance().loadProperties();
		}
		catch( Exception ex ) {
			Logger.getLogger(SingleServer.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(1);
		}

		// add Accounts
		// ---------------------------------------------------------------------

		SimpleAccountManager am = SimpleAccountManager.getInstance();

		// <sample how can add a account>
		// IAccount test1Acc = am.addAccount("test", "test");

		// world manager
		// ---------------------------------------------------------------------

		SimpleWorldManager wm = SimpleWorldManager.getInstance();

		// character manager
		// ---------------------------------------------------------------------

		SimpleCharacterManager cm = SimpleCharacterManager.getInstance();


		// zone manager
		// ---------------------------------------------------------------------
		SimpleZoneManager zm = SimpleZoneManager.getInstance();

		// guild manager
		// ---------------------------------------------------------------------
		GuildManager gm = (GuildManager) GuildManager.getInstance();

		gm.addGuild(new Guild(Guild.DEV_GUILD_ID, "Dev"));
		gm.addGuild(new Guild(Guild.GM_GUILD_ID, "GameMaster"));

		// ---------------------------------------------------------------------

		SingleServer ss = new SingleServer(config);
		ss.start();
	}
}