/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.simple.accounts;

import com.fjestaserver.core.account.Account;
import java.util.Properties;

/**
 * SimpleAccount
 * @author FantaBlueMystery
 */
public class SimpleAccount extends Account {

	/**
	 * KEY account id
	 */
	static public String ID = "id";

	/**
	 * KEY account username
	 */
	static public String USERNAME = "username";

	/**
	 * KEY account password
	 */
	static public String PASSWORD = "password";

	/**
	 * properties
	 */
	protected Properties _properties = null;

	/**
	 * SimpleAccount
	 * @param id
	 * @param username
	 * @param password
	 */
	public SimpleAccount(int id, String username, String password) {
		this._id		= id;
		this._username	= username;
		this._password	= password;

		this._properties = new Properties();
		this._properties.setProperty(SimpleAccount.ID, Integer.toString(id));
		this._properties.setProperty(SimpleAccount.USERNAME, username);
		this._properties.setProperty(SimpleAccount.PASSWORD, password);
	}

	/**
	 * SimpleAccount
	 * @param accountProperties
	 */
	public SimpleAccount(Properties accountProperties) {
		this._properties = accountProperties;

		this._id		= Integer.valueOf(this._properties.getProperty(SimpleAccount.ID, "0"));
		this._username	= this._properties.getProperty(SimpleAccount.USERNAME, "unknow");
		this._password	= this._properties.getProperty(SimpleAccount.PASSWORD, "");
	}

	/**
	 * getProperties
	 * @return
	 */
	public Properties getProperties() {
		return this._properties;
	}
}