/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.simple.accounts;

import com.fjestaserver.core.account.AccountManager;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.simple.config.SimpleConfig;
import com.fjestaserver.simple.config.SimpleConfigLoader;
import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SimpleAccountManager
 * @author FantaBlueMystery
 */
public class SimpleAccountManager extends AccountManager {

	/**
	 * i
	 * return instance of account manager
	 * @return
	 */
	static public SimpleAccountManager getInstance() {
		if( AccountManager._instance == null ) {
			AccountManager._instance = new SimpleAccountManager();
		}

		return (SimpleAccountManager) AccountManager._instance;
	}

	/**
	 * SimpleAccountManager
	 */
	public SimpleAccountManager() {
		try {
			SimpleConfig config = (SimpleConfig) SimpleConfigLoader.getInstance().loadProperties();

			String pathAccounts = config.getPathAccounts();

			if( !pathAccounts.isEmpty() ) {
				File fpAccounts = new File(pathAccounts);

				File[] files = fpAccounts.listFiles();

				for( File afile: files ) {
					Properties aprop = SimpleConfigLoader.getInstance().loadProperties(afile);

					this.addAccount(new SimpleAccount(aprop));
				}
			}
			else {
				System.out.println("SimpleAccountManager: None config files found for Accounts! Please setup your config files.");
			}
		}
		catch( Exception ex ) {
			Logger.getLogger(SimpleAccountManager.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * addAccount
	 * @param username
	 * @param password
	 * @return
	 */
	public IAccount addAccount(String username, String password) {
		IAccount na = new SimpleAccount(
			(this.count()+1),
			username,
			password
			);

		this.addAccount(na);

		System.out.println("SimpleAccountManager: add account" +
			" Id: " + Integer.toString(na.getId()) +
			" Name: " + na.getUsername()
			);

		return na;
	}

	/**
	 * addAccount
	 * @param account
	 */
	@Override
	public void addAccount(IAccount account) {
		super.addAccount(account);

		System.out.println("SimpleAccountManager: add account" +
			" Id: " + Integer.toString(account.getId()) +
			" Name: " + account.getUsername()
			);
	}
}