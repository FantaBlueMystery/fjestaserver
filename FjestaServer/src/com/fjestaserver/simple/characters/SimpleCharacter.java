/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.simple.characters;

import com.fjestaserver.core.characters.CharacterServer;

/**
 * SimpleCharacter
 * @author FantaBlueMystery
 */
public class SimpleCharacter extends CharacterServer {

}