/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.simple.characters;

import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.characters.CharacterExceptionMax;
import com.fjestaserver.core.characters.CharacterManager;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.guild.Guild;

/**
 * SimpleCharacterManager
 * @author FantaBlueMystery
 */
public class SimpleCharacterManager extends CharacterManager {

	/**
	 * getInstance
	 * return instance of character manager
	 * @return
	 */
	static public SimpleCharacterManager getInstance() {
		if( !(CharacterManager._instance instanceof SimpleCharacterManager) ) {
			CharacterManager._instance = new SimpleCharacterManager();
		}

		return (SimpleCharacterManager) CharacterManager._instance;
	}

	/**
	 * addCharacter
	 * @param account
	 * @param character
	 * @return
	 * @throws CharacterExceptionMax
	 */
	@Override
	public ICharacterServer addCharacter(IAccount account, ICharacterServer character) throws CharacterExceptionMax {
		character.setGuildId(Guild.DEV_GUILD_ID);

		return super.addCharacter(account, character);
	}
}