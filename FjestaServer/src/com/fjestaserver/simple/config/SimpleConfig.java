/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.simple.config;

import com.fjestacore.core.config.GlobalConfig;
import com.fjestacore.socket.config.SocketConfig;
import com.fjestaserver.login.config.LoginConfig;
import com.fjestaserver.world.config.WorldConfig;
import com.fjestaserver.zone.config.ZoneConfig;

/**
 * SimpleConfig
 * @author FantaBlueMystery
 */
public class SimpleConfig extends GlobalConfig {

	/**
	 * KEY login port
	 */
	static public String LOGIN_PORT = "login_port";

	/**
	 * KEY login debug
	 */
	static public String LOGIN_DEBUG = "login_debug";

	/**
	 * KEY login binding address
	 */
	static public String LOGIN_BINDING_ADDRESS = "login_binding_address";

	/**
	 * KEY login check client version
	 */
	static public String LOGIN_CHECK_CLIENT_VERSION = "login_check_client_version";

	/**
	 * KEY login world id
	 */
	static public String LOGIN_WORLD_ID = "login_world_id";

	/**
	 * KEY login world status
	 */
	static public String LOGIN_WORLD_STATUS = "login_world_status";

	/**
	 * KEY login world ip
	 */
	static public String LOGIN_WORLD_IP = "login_world_ip";

	/**
	 * KEY login world port
	 */
	static public String LOGIN_WORLD_PORT = "login_world_port";

	/**
	 * KEY world port
	 */
	static public String WORLD_PORT = "world_port";

	/**
	 * KEY world debug
	 */
	static public String WORLD_DEBUG = "world_debug";

	/**
	 * KEY world binding address
	 */
	static public String WORLD_BINDING_ADDRESS = "world_binding_address";

	/**
	 * KEY world zone indexname
	 */
	static public String WORLD_ZONE_INDEXNAME = "world_zone_indexname";

	/**
	 * KEY world zone ip
	 */
	static public String WORLD_ZONE_IP = "world_zone_ip";

	/**
	 * KEY world zone port
	 */
	static public String WORLD_ZONE_PORT = "world_zone_port";

	/**
	 * KEY zone port
	 */
	static public String ZONE_PORT = "zone_port";

	/**
	 * KEY zone debug
	 */
	static public String ZONE_DEBUG = "zone_debug";

	/**
	 * KEY zone binding address
	 */
	static public String ZONE_BINDING_ADDRESS = "zone_binding_address";

	/**
	 * KEY path to accounts (Properties files)
	 */
	static public String PATH_ACCOUNTS = "path_accounts";

	/**
	 * KEY extern ip
	 */
	static public String EXTERN_IP = "extern_ip";

	/**
	 * getLoginPortStr
	 * @return
	 */
	public String getLoginPortStr() {
		return this.getProperty(SimpleConfig.LOGIN_PORT, "9010");
	}

	/**
	 * getLoginPort
	 * @return
	 */
	public int getLoginPort() {
		return Integer.valueOf(this.getLoginPortStr());
	}

	/**
	 * getLoginDebugStr
	 * @return
	 */
	public String getLoginDebugStr() {
		return this.getProperty(SimpleConfig.LOGIN_DEBUG, "0");
	}

	/**
	 * getLoginBindingAddress
	 * @return
	 */
	public String getLoginBindingAddress() {
		return this.getProperty(SimpleConfig.LOGIN_BINDING_ADDRESS, "0.0.0.0");
	}

	/**
	 * getLoginCheckClientVersionStr
	 * @return
	 */
	public String getLoginCheckClientVersionStr() {
		return this.getProperty(SimpleConfig.LOGIN_CHECK_CLIENT_VERSION, "1");
	}

	/**
	 * getLoginWorldId
	 * @return
	 */
	public String getLoginWorldId() {
		return this.getProperty(SimpleConfig.LOGIN_WORLD_ID, "GFT-GRM0");
	}

	/**
	 * getLoginWorldStatus
	 * @return
	 */
	public int getLoginWorldStatus() {
		return Integer.valueOf(this.getProperty(SimpleConfig.LOGIN_WORLD_STATUS, "1"));
	}

	/**
	 * getLoginWorldIp
	 * @return
	 */
	public String getLoginWorldIp() {
		return this.getProperty(SimpleConfig.LOGIN_WORLD_IP, "127.0.0.1");
	}

	/**
	 * getLoginWorldPort
	 * @return
	 */
	public int getLoginWorldPort() {
		return Integer.valueOf(this.getProperty(SimpleConfig.LOGIN_WORLD_PORT, "9020"));
	}

	/**
	 * getLoginConfig
	 * @return
	 */
	public LoginConfig getLoginConfig() {
		LoginConfig sc = new LoginConfig();

		sc.setProperty(SocketConfig.PORT, this.getLoginPortStr());
		sc.setProperty(SocketConfig.DEBUG, this.getLoginDebugStr());
		sc.setProperty(SocketConfig.BINDING_ADDRESS, this.getLoginBindingAddress());
		sc.setProperty(LoginConfig.CHECK_CLIENT_VERSION, this.getLoginCheckClientVersionStr());

		return sc;
	}

	/**
	 * getWorldPortStr
	 * @return
	 */
	public String getWorldPortStr() {
		return this.getProperty(SimpleConfig.WORLD_PORT, "9020");
	}

	/**
	 * getWorldPort
	 * @return
	 */
	public int getWorldPort() {
		return Integer.valueOf(this.getWorldPortStr());
	}

	/**
	 * getWorldDebugStr
	 * @return
	 */
	public String getWorldDebugStr() {
		return this.getProperty(SimpleConfig.WORLD_DEBUG, "0");
	}

	/**
	 * getWorldBindingAddress
	 * @return
	 */
	public String getWorldBindingAddress() {
		return this.getProperty(SimpleConfig.WORLD_BINDING_ADDRESS, "0.0.0.0");
	}

	/**
	 * getWorldZoneIndexname
	 * @return
	 */
	public String getWorldZoneIndexname() {
		return this.getProperty(SimpleConfig.WORLD_ZONE_INDEXNAME, "Eld");
	}

	/**
	 * getWorldZoneIp
	 * @return
	 */
	public String getWorldZoneIp() {
		return this.getProperty(SimpleConfig.WORLD_ZONE_IP, "127.0.0.1");
	}

	/**
	 * getWorldZonePort
	 * @return
	 */
	public int getWorldZonePort() {
		return Integer.valueOf(this.getProperty(SimpleConfig.WORLD_ZONE_PORT, "9100"));
	}

	/**
	 * getWorldConfig
	 * @return
	 */
	public WorldConfig getWorldConfig() {
		WorldConfig wc = new WorldConfig();

		wc.setProperty(SocketConfig.PORT, this.getWorldPortStr());
		wc.setProperty(SocketConfig.DEBUG, this.getWorldDebugStr());
		wc.setProperty(SocketConfig.BINDING_ADDRESS, this.getWorldBindingAddress());

		return wc;
	}

	/**
	 * getZonePortStr
	 * @return
	 */
	public String getZonePortStr() {
		return this.getProperty(SimpleConfig.ZONE_PORT, "9100");
	}

	/**
	 * getZoneDebugStr
	 * @return
	 */
	public String getZoneDebugStr() {
		return this.getProperty(SimpleConfig.ZONE_DEBUG, "0");
	}

	/**
	 * getZoneBindingAddress
	 * @return
	 */
	public String getZoneBindingAddress() {
		return this.getProperty(SimpleConfig.ZONE_BINDING_ADDRESS, "0.0.0.0");
	}

	/**
	 * getZoneConfig
	 * @return
	 */
	public ZoneConfig getZoneConfig() {
		ZoneConfig zc = new ZoneConfig();

		zc.setProperty(SocketConfig.PORT, this.getZonePortStr());
		zc.setProperty(SocketConfig.DEBUG, this.getZoneDebugStr());
		zc.setProperty(SocketConfig.BINDING_ADDRESS, this.getZoneBindingAddress());

		return zc;
	}

	/**
	 * getPathAccounts
	 * @return
	 */
	public String getPathAccounts() {
		return this._getPathByLoaction(SimpleConfig.PATH_ACCOUNTS, "");
	}
}