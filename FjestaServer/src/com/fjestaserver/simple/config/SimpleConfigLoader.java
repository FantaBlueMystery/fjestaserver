/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.simple.config;

import com.fjestacore.core.config.DefaultConfigLoader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * SimpleConfigLoader
 * @author FantaBlueMystery
 */
public class SimpleConfigLoader extends DefaultConfigLoader {

	/**
	 * Consts
	 */
	static public String PROPERTY_FILE	= "simpleserver.properties";

	/**
	 * SimpleConfigLoader
	 * @param pGlobalFile
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public SimpleConfigLoader(String pGlobalFile) throws FileNotFoundException, IOException {
		super(pGlobalFile);
	}

	/**
	 * _createProperties
	 * @return
	 */
	@Override
	protected Properties _createProperties() {
		return new SimpleConfig();
	}
}