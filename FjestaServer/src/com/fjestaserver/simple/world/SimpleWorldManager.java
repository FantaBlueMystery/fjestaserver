/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestaserver.simple.world;

import com.fjestacore.common.world.IWorld;
import com.fjestacore.common.world.World;
import com.fjestacore.common.world.WorldName;
import com.fjestacore.common.world.WorldStatus;
import com.fjestaserver.core.world.WorldManager;
import com.fjestaserver.simple.config.SimpleConfig;
import com.fjestaserver.simple.config.SimpleConfigLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SimpleWorldManager
 * @author FantaBlueMystery
 */
public class SimpleWorldManager extends WorldManager {

	/**
	 * getInstance
	 * return instance of world manager
	 * @return
	 */
	static public SimpleWorldManager getInstance() {
		if( WorldManager._instance == null ) {
			WorldManager._instance = new SimpleWorldManager();
		}

		return (SimpleWorldManager) WorldManager._instance;
	}

	/**
	 * SimpleWorldManager
	 * full default all server to MAINTENANCE
	 */
	public SimpleWorldManager() {
		WorldName wnames[] = WorldName.values();

		int index = 0;

		for( WorldName wname: wnames ) {
			index++;

			this._wl.add(new World(index, wname, WorldStatus.MAINTENANCE, "", 0));
		}

		try {
			SimpleConfig config = (SimpleConfig) SimpleConfigLoader.getInstance().loadProperties();

			IWorld tworld = this.getWorld(WorldName.getWorldByName(config.getLoginWorldId()));

			if( tworld != null ) {
				tworld.setStatus(config.getLoginWorldStatus());
				tworld.setIp(config.getLoginWorldIp());
				tworld.setPort(config.getLoginWorldPort());

				System.out.println("SimpleWorldManager: set world" +
					" Name: " + tworld.getName() +
					" IP: " + tworld.getIp() +
					" Port: " + Integer.toString(tworld.getPort())
					);
			}
			else {
				System.out.println("SimpleWorldManager: World not found by: " +
					config.getLoginWorldId() + "! Please setup your config files!");
			}
		}
		catch( Exception ex ) {
			Logger.getLogger(SimpleWorldManager.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * getWorld
	 * @param wn
	 * @return
	 */
	public World getWorld(WorldName wn) {
		for( IWorld _world : this._wl.getList() ) {
			if( _world.getName().equals(wn.getValue()) ) {
				return (World) _world;
			}
		}

		return null;
	}

	/**
	 * addWorld
	 * @param world
	 */
	public void addWorld(World world) {
		this._wl.add(world);
	}
}