/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.simple.zone;

import com.fjestaserver.core.zone.Zone;

/**
 * SimpleZone
 * @author FantaBlueMystery
 */
public class SimpleZone extends Zone {

	/**
	 * SimpleZone
	 * @param id
	 * @param indexName
	 * @param serverIP
	 * @param serverPort
	 */
	public SimpleZone(int id, String indexName, String serverIP, int serverPort) {
		this._id			= id;
		this._indexName		= indexName;
		this._serverIp		= serverIP;
		this._serverPort	= serverPort;
	}
}
