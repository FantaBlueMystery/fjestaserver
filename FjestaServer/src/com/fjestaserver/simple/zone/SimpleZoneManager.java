/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.simple.zone;

import com.fjestaserver.core.zone.ZoneManager;
import com.fjestaserver.simple.config.SimpleConfig;
import com.fjestaserver.simple.config.SimpleConfigLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SimpleZoneManager
 * @author FantaBlueMystery
 */
public class SimpleZoneManager extends ZoneManager {

	/**
	 * getInstance
	 * return instance of zone manager
	 * @return
	 */
	static public SimpleZoneManager getInstance() {
		if( ZoneManager._instance == null ) {
			ZoneManager._instance = new SimpleZoneManager();
		}

		return (SimpleZoneManager) ZoneManager._instance;
	}

	/**
	 * SimpleZoneManager
	 */
	public SimpleZoneManager() {
		try {
			SimpleConfig config = (SimpleConfig) SimpleConfigLoader.getInstance().loadProperties();

			this._zones.add(new SimpleZone(0,
				config.getWorldZoneIndexname(),
				config.getWorldZoneIp(),
				config.getWorldZonePort()
				));
		}
		catch( Exception ex ) {
			Logger.getLogger(SimpleZoneManager.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
