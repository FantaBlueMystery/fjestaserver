/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world;

import com.fjestacore.socket.ServerBase;
import com.fjestaserver.core.net.Client;
import java.net.Socket;

/**
 * ClientWorld
 * @author FantaBlueMystery
 */
public class ClientWorld extends Client {

	/**
	 * ClientWorld
	 * @param server
	 * @param clientSocket
	 */
	public ClientWorld(ServerBase server, Socket clientSocket) {
		super(server, clientSocket);
	}

	/**
	 * _getClassName
	 * @return
	 */
	@Override
	protected String _getClassName() {
		return this.getClass().getName().replace("com.fjestaserver.world.", "");
	}

	/**
	 * getServer
	 * @return
	 */
	@Override
	public ServerWorld getServer() {
		return (ServerWorld) this._server;
	}
}