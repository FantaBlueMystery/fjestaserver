/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world;

import com.fjestacore.core.net.ServerType;
import com.fjestaserver.core.net.Server;
import com.fjestaserver.handler.HandlerDictionary;
import com.fjestaserver.world.config.WorldConfig;
import com.fjestaserver.world.handler.H02Misc;
import com.fjestaserver.world.handler.H03User;
import com.fjestaserver.world.handler.H04Char;
import com.fjestaserver.world.handler.H05Avatar;
import com.fjestaserver.world.handler.H28CharOption;
import com.fjestaserver.world.handler.H29Guild;
import com.fjestaserver.world.handler.H31Prison;
import com.fjestaserver.world.handler.H48Mid;
import java.net.Socket;

/**
 * ServerWorld
 * @author FantaBlueMystery
 */
public class ServerWorld extends Server {

	/**
	 * ServerWorld
	 * @param prop
	 */
	public ServerWorld(WorldConfig prop) {
		super(prop);

		this._serverType = ServerType.WORLD;

		HandlerDictionary.getInstance().addHandler(H02Misc.class);
		HandlerDictionary.getInstance().addHandler(H03User.class);
		HandlerDictionary.getInstance().addHandler(H04Char.class);
		HandlerDictionary.getInstance().addHandler(H05Avatar.class);
		HandlerDictionary.getInstance().addHandler(H28CharOption.class);
		HandlerDictionary.getInstance().addHandler(H29Guild.class);
		HandlerDictionary.getInstance().addHandler(H31Prison.class);
		HandlerDictionary.getInstance().addHandler(H48Mid.class);
	}

	/**
	 * getProperties
	 * @return
	 */
	@Override
	public WorldConfig getProperties() {
		return (WorldConfig) this._properties;
	}

	/**
	 * _castSocket
	 * @param cs
	 * @return
	 */
	@Override
	protected ClientWorld _castSocket(Socket cs) {
		return new ClientWorld(this, cs);
	}
}