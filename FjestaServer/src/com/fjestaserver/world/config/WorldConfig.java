/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.config;

import com.fjestacore.socket.config.SocketConfig;

/**
 * WorldConfig
 * @author FantaBlueMystery
 */
public class WorldConfig extends SocketConfig {

	/**
	 * KEY item shop url
	 */
	static public String ITEM_SHOP_URL = "item_shop_url";

	/**
	 * getItemShopUrl
	 * @return
	 */
	public String getItemShopUrl() {
		return this.getProperty(WorldConfig.ITEM_SHOP_URL, "https://www.google.de");
	}
}