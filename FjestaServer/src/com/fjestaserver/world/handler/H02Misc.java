/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.handler;

import com.fjestacore.common.misc.ItemshopUrlAck;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.misc.NC_MISC_GAMETIME_ACK;
import com.fjestacore.packet.misc.NC_MISC_GAMETIME_REQ;
import com.fjestacore.packet.misc.NC_MISC_ITEMSHOP_URL_ACK;
import com.fjestacore.packet.misc.NC_MISC_ITEMSHOP_URL_REQ;
import com.fjestacore.server.ServerError;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.world.ClientWorld;
import com.fjestaserver.world.ServerWorld;
import com.fjestaserver.world.config.WorldConfig;

/**
 * H02Misc
 * @author FantaBlueMystery
 */
public class H02Misc {

	/**
	 * NC_MISC_GAMETIME_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_MISC_GAMETIME_REQ)
	static public void NC_MISC_GAMETIME_REQ(ClientWorld client, NC_MISC_GAMETIME_REQ packet) {
		client.writePacket(new NC_MISC_GAMETIME_ACK());
	}

	/**
	 * NC_MISC_ITEMSHOP_URL_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_MISC_ITEMSHOP_URL_REQ)
	static public void NC_MISC_ITEMSHOP_URL_REQ(ClientWorld client, NC_MISC_ITEMSHOP_URL_REQ packet) {
		ServerWorld sw = client.getServer();
		WorldConfig wc = sw.getProperties();

		String url = wc.getItemShopUrl();

		if( url.equals("") ) {
			client.writePacket(new NC_MISC_ITEMSHOP_URL_ACK(
				new ItemshopUrlAck(ServerError.ERROR_OPTIONS)));
		}
		else {
			client.writePacket(new NC_MISC_ITEMSHOP_URL_ACK(
				new ItemshopUrlAck(ServerError.NONE, url)));
		}
	}
}
