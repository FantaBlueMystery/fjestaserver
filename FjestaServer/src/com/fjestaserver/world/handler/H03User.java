/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.handler;

import com.fjestacore.common.avatar.AvatarInformationList;
import com.fjestacore.common.login.UserWorldLogin;
import com.fjestacore.common.user.UserWillWorldSelect;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.user.NC_USER_AVATAR_LIST_REQ;
import com.fjestacore.packet.user.NC_USER_LOGINWORLD_ACK;
import com.fjestacore.packet.user.NC_USER_LOGINWORLD_REQ;
import com.fjestacore.packet.user.NC_USER_WILL_WORLD_SELECT_ACK;
import com.fjestacore.packet.user.NC_USER_WILL_WORLD_SELECT_REQ;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.characters.CharacterManager;
import com.fjestaserver.core.characters.ICharacterManager;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.handle.HandleManager;
import com.fjestaserver.core.handle.IHandleManager;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.core.session.ISessionManager;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.world.ClientWorld;
import com.fjestaserver.core.session.SessionManager;
import java.util.logging.Level;

/**
 * H03User
 * @author FantaBlueMystery
 */
public class H03User {

	/**
	 * NC_USER_LOGINWORLD_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_USER_LOGINWORLD_REQ)
	static public void NC_USER_LOGINWORLD_REQ(ClientWorld client, NC_USER_LOGINWORLD_REQ packet) {
		ISessionManager sm = SessionManager.getInstance();
		ISession session = sm.getSession(packet.getLogin().getSession());

		if( session == null ) {
			client.log(Level.WARNING, "session is not set, close connection.");

			client.getServer().removeClient(client);
			return;
		}

		// ---------------------------------------------------------------------

		IAccount account = session.getAccount();

		if( account == null ) {
			client.log(Level.WARNING, "Account is not set, close session and connection.");

			sm.closeSession(session);
			client.getServer().removeClient(client);
			return;
		}

		if( account.getUsername().compareTo(packet.getLogin().getUsername()) != 0 ) {
			client.log(Level.WARNING, "username unequal, close session and connection.");

			sm.closeSession(session);
			client.getServer().removeClient(client);
			return;
		}

		// ---------------------------------------------------------------------

		ICharacterManager cm = CharacterManager.getInstance();

		if( cm == null ) {
			sm.closeSession(session);
			client.getServer().removeClient(client);
			return;
		}

		// set session for world client
		client.setSession(session);

		IHandleManager hm = HandleManager.getInstance();
		session.setWorldHandle((int) hm.getFreeWorldHandle());

		AvatarInformationList csl = new AvatarInformationList();

		for( ICharacterServer acharacter: cm.getCharacters(account) ) {
			csl.addCharacter(acharacter.getAvatar());
		}

		client.writePacket(new NC_USER_LOGINWORLD_ACK(
			new UserWorldLogin(session.getWorldHandle(), csl)));
	}

	/**
	 * NC_USER_AVATAR_LIST_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_USER_AVATAR_LIST_REQ)
	static public void NC_USER_AVATAR_LIST_REQ(ClientWorld client, NC_USER_AVATAR_LIST_REQ packet) {
		ISession session = client.getSession();

		if( session != null ) {
			ICharacterManager cm = CharacterManager.getInstance();

			if( cm != null ) {
				IAccount account = session.getAccount();

				AvatarInformationList csl = new AvatarInformationList();

				for( ICharacterServer acharacter: cm.getCharacters(account) ) {
					csl.addCharacter(acharacter.getAvatar());
				}

				client.writePacket(new NC_USER_LOGINWORLD_ACK(
					new UserWorldLogin(session.getWorldHandle(), csl)));

				return;
			}
		}

		client.getServer().removeClient(client);
	}

	/**
	 * NC_USER_WILL_WORLD_SELECT_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_USER_WILL_WORLD_SELECT_REQ)
	static public void NC_USER_WILL_WORLD_SELECT_REQ(ClientWorld client, NC_USER_WILL_WORLD_SELECT_REQ packet) {
		ISession session = client.getSession();

		int nError = 7768;
		String otp = "";

		if( session == null ) {
			client.log(Level.WARNING, "session is not set, close connection.");

			nError = 73;
		}
		else {
			otp = session.getOtp(true);
		}

		client.writePacket(new NC_USER_WILL_WORLD_SELECT_ACK(
			new UserWillWorldSelect(nError, otp)));
	}
}