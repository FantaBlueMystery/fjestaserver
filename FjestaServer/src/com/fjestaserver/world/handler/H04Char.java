/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.handler;

import com.fjestacore.common.character.CharLoginAck;
import com.fjestacore.common.character.CharLoginFail;
import com.fjestacore.common.map.MapDefault;
import com.fjestacore.common.map.MapInfo;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.char_options.NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD;
import com.fjestacore.packet.char_options.NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD;
import com.fjestacore.packet.char_options.NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD;
import com.fjestacore.packet.chars.NC_CHAR_LOGINFAIL_ACK;
import com.fjestacore.packet.chars.NC_CHAR_LOGIN_ACK;
import com.fjestacore.packet.chars.NC_CHAR_LOGIN_REQ;
import com.fjestacore.packet.misc.NC_MISC_SERVER_TIME_NOTIFY_CMD;
import com.fjestacore.server.ServerError;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.characters.CharacterManager;
import com.fjestaserver.core.characters.CharacterServer;
import com.fjestaserver.core.characters.ICharacterManager;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.core.zone.IZone;
import com.fjestaserver.core.zone.IZoneManager;
import com.fjestaserver.core.zone.ZoneManager;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.world.ClientWorld;

/**
 * H04Char
 * @author FantaBlueMystery
 */
public class H04Char {

	/**
	 * NC_CHAR_LOGIN_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_CHAR_LOGIN_REQ)
	static public void NC_CHAR_LOGIN_REQ(ClientWorld client, NC_CHAR_LOGIN_REQ packet) {
		ISession session = client.getSession();

		if( session == null ) {
			client.writePacket(new NC_CHAR_LOGINFAIL_ACK(new CharLoginFail(ServerError.UNKNOWN_ERROR)));
			return;
		}

		IAccount account = session.getAccount();

		if( account == null ) {
			client.writePacket(new NC_CHAR_LOGINFAIL_ACK(new CharLoginFail(ServerError.UNKNOWN_ERROR)));
			return;
		}

		ICharacterManager cm = CharacterManager.getInstance();

		if( cm == null ) {
			client.writePacket(new NC_CHAR_LOGINFAIL_ACK(new CharLoginFail(ServerError.UNKNOWN_ERROR)));
			return;
		}

		ICharacterServer character = cm.getCharacter(account, packet.getCharLogin().getSlot());

		if( character == null ) {
			client.writePacket(new NC_CHAR_LOGINFAIL_ACK(new CharLoginFail(ServerError.UNKNOWN_ERROR)));
			return;
		}

		CharacterServer scharacter = (CharacterServer) character;
		String mapname = scharacter.getMapname();

		if( true ) {
			MapInfo mapInfo = MapDefault.getMapInfo();

			mapname = mapInfo.getMapName();

			scharacter.setMapname(mapname);
			scharacter.getPosition().setX(mapInfo.getRegenX());
			scharacter.getPosition().setY(mapInfo.getRegenY());
		}

		IZoneManager zm = ZoneManager.getInstance();

		if( zm == null ) {
			client.writePacket(new NC_CHAR_LOGINFAIL_ACK(new CharLoginFail(ServerError.UNKNOWN_ERROR)));
			return;
		}

		IZone zone = zm.getZone(mapname);

		if( zone == null ) {
			client.writePacket(new NC_CHAR_LOGINFAIL_ACK(new CharLoginFail(ServerError.UNKNOWN_ERROR)));
			return;
		}

		session.setCharacter(scharacter);

		// send zone login
		client.writePacket(new NC_CHAR_LOGIN_ACK(
			new CharLoginAck(zone.getServerIP(), zone.getServerPort())));

		// TODO
		client.writePacket(new NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD());
		client.writePacket(new NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD());
		client.writePacket(new NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD());
		client.writePacket(new NC_MISC_SERVER_TIME_NOTIFY_CMD());
	}
}