/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.handler;

import com.fjestacore.common.avatar.AvatarCreateFail;
import com.fjestacore.common.avatar.AvatarCreateSucc;
import com.fjestacore.common.avatar.AvatarEraseFail;
import com.fjestacore.common.avatar.AvatarEraseSucc;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.avatar.NC_AVATAR_CREATEFAIL_ACK;
import com.fjestacore.packet.avatar.NC_AVATAR_CREATESUCC_ACK;
import com.fjestacore.packet.avatar.NC_AVATAR_CREATE_REQ;
import com.fjestacore.packet.avatar.NC_AVATAR_ERASEFAIL_ACK;
import com.fjestacore.packet.avatar.NC_AVATAR_ERASESUCC_ACK;
import com.fjestacore.packet.avatar.NC_AVATAR_ERASE_REQ;
import com.fjestacore.server.ServerError;
import com.fjestaserver.core.account.IAccount;
import com.fjestaserver.core.characters.CharacterExceptionMax;
import com.fjestaserver.core.characters.CharacterManager;
import com.fjestaserver.core.characters.CharacterServer;
import com.fjestaserver.core.characters.ICharacterManager;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.core.word.IWordFilterManager;
import com.fjestaserver.core.word.WordFilterManager;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.world.ClientWorld;
import java.util.logging.Level;

/**
 * H05Avatar
 * @author FantaBlueMystery
 */
public class H05Avatar {

	/**
	 * NC_AVATAR_CREATE_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_AVATAR_CREATE_REQ)
	static public void NC_AVATAR_CREATE_REQ(ClientWorld client, NC_AVATAR_CREATE_REQ packet) {
		ISession session = client.getSession();

		if( session != null ) {
			IAccount account = session.getAccount();

			if( account != null ) {
				String charname = packet.getCreate().getCharname();

				if( charname.length() <= 3 ) {
					client.log(Level.WARNING, "name to short: " + charname);

						client.writePacket(new NC_AVATAR_CREATEFAIL_ACK(
							new AvatarCreateFail(ServerError.NAME_TAKEN)));

						return;
				}

				IWordFilterManager wfm = WordFilterManager.getInstance();

				if( wfm != null ) {
					if( wfm.isFoundBadWord(charname) ) {
						client.log(Level.WARNING, "bad name found: " + charname);

						client.writePacket(new NC_AVATAR_CREATEFAIL_ACK(
							new AvatarCreateFail(ServerError.NAME_TAKEN)));

						return;
					}
				}
				else {
					client.log(Level.WARNING, "world filter manager instance is not found!");
					client.writePacket(new NC_AVATAR_CREATEFAIL_ACK(
						new AvatarCreateFail(ServerError.FAILED_TO_CREATE)));

					return;
				}

				ICharacterManager cm = CharacterManager.getInstance();

				if( cm.existCharacter(packet.getCreate().getCharname()) ) {
					client.writePacket(new NC_AVATAR_CREATEFAIL_ACK(
						new AvatarCreateFail(ServerError.NAME_IN_USE)));

					return;
				}

				if( cm.existCharacter(account, packet.getCreate().getSlot()) ) {
					client.writePacket(new NC_AVATAR_CREATEFAIL_ACK(
						new AvatarCreateFail(ServerError.ERROR_IN_MAX_SLOT)));
					return;
				}

				try {
					CharacterServer cs = new CharacterServer();

					cs.setLook(packet.getCreate().getLook());
					cs.setCharname(packet.getCreate().getCharname());

					CharacterServer ncs = (CharacterServer) cm.addCharacter(account, cs);

					client.writePacket(new NC_AVATAR_CREATESUCC_ACK(
						new AvatarCreateSucc(cm.countCharacter(account), ncs.getAvatar())));
				}
				catch( CharacterExceptionMax emc ) {
					client.writePacket(new NC_AVATAR_CREATEFAIL_ACK(
						new AvatarCreateFail(ServerError.ERROR_IN_MAX_SLOT)));
				}
				catch( Exception ex ) {
					client.writePacket(new NC_AVATAR_CREATEFAIL_ACK(
						new AvatarCreateFail(ServerError.FAILED_TO_CREATE)));
				}
			}
			else {
				client.log(Level.WARNING, "account is not set, close connection.");
				client.getServer().removeClient(client);
			}
		}
		else {
			client.log(Level.WARNING, "session is not set, close connection.");
			client.getServer().removeClient(client);
		}
	}

	/**
	 * NC_AVATAR_ERASE_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_AVATAR_ERASE_REQ)
	static public void NC_AVATAR_ERASE_REQ(ClientWorld client, NC_AVATAR_ERASE_REQ packet) {
		ISession session = client.getSession();

		if( session != null ) {
			IAccount account = session.getAccount();

			if( account != null ) {
				int slot = packet.getAvatarErase().getSlot();

				ICharacterManager cm = CharacterManager.getInstance();

				if( cm.existCharacter(account, slot) ) {
					if( cm.eraseCharacter(account, slot) ) {
						client.writePacket(new NC_AVATAR_ERASESUCC_ACK(new AvatarEraseSucc(slot)));
						return;
					}
				}

				client.writePacket(new NC_AVATAR_ERASEFAIL_ACK(
					new AvatarEraseFail(ServerError.ERROR_CHAR_INFO)));
			}
			else {
				client.log(Level.WARNING, "account is not set, close connection.");
				client.getServer().removeClient(client);
			}
		}
		else {
			client.log(Level.WARNING, "session is not set, close connection.");
			client.getServer().removeClient(client);
		}
	}
}