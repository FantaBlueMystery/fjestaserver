/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.char_options.NC_CHAR_OPTION_GET_SHORTCUTSIZE_ACK;
import com.fjestacore.packet.char_options.NC_CHAR_OPTION_GET_SHORTCUTSIZE_REQ;
import com.fjestacore.packet.char_options.NC_CHAR_OPTION_GET_WINDOWPOS_ACK;
import com.fjestacore.packet.char_options.NC_CHAR_OPTION_GET_WINDOWPOS_REQ;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.world.ClientWorld;

/**
 * H28CharOption
 * @author FantaBlueMystery
 */
public class H28CharOption {

	/**
	 * NC_CHAR_OPTION_GET_SHORTCUTSIZE_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_CHAR_OPTION_GET_SHORTCUTSIZE_REQ)
	static public void NC_CHAR_OPTION_GET_SHORTCUTSIZE_REQ(ClientWorld client, NC_CHAR_OPTION_GET_SHORTCUTSIZE_REQ packet) {
		client.writePacket(new NC_CHAR_OPTION_GET_SHORTCUTSIZE_ACK());
	}

	/**
	 * NC_CHAR_OPTION_GET_WINDOWPOS_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_CHAR_OPTION_GET_WINDOWPOS_REQ)
	static public void NC_CHAR_OPTION_GET_WINDOWPOS_REQ(ClientWorld client, NC_CHAR_OPTION_GET_WINDOWPOS_REQ packet) {
		client.writePacket(new NC_CHAR_OPTION_GET_WINDOWPOS_ACK());
	}
}
