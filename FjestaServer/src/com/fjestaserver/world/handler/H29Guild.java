/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.handler;

import com.fjestacore.common.guild.GuildNameAck;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.guild.NC_GUILD_NAME_ACK;
import com.fjestacore.packet.guild.NC_GUILD_NAME_REQ;
import com.fjestaserver.core.guild.GuildManager;
import com.fjestaserver.core.guild.IGuild;
import com.fjestaserver.core.guild.IGuildManager;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.world.ClientWorld;

/**
 * H29Guild
 * @author FantaBlueMystery
 */
public class H29Guild {

	/**
	 * NC_GUILD_NAME_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_GUILD_NAME_REQ)
	static public void NC_GUILD_NAME_REQ(ClientWorld client, NC_GUILD_NAME_REQ packet) {
		ISession session = client.getSession();

		if( session != null ) {
			IGuildManager gm = GuildManager.getInstance();

			IGuild g = gm.getGuild(packet.getGuildNameReq().getGuildId());

			if( g != null ) {
				client.writePacket(new NC_GUILD_NAME_ACK(new GuildNameAck(
					g.getId(),
					g.getName()
					)));
			}
		}
	}
}