/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.prison.NC_PRISON_GET_ACK;
import com.fjestacore.packet.prison.NC_PRISON_GET_REQ;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.world.ClientWorld;

/**
 * H31Prison
 * @author FantaBlueMystery
 */
public class H31Prison {

	/**
	 * NC_PRISON_GET_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_PRISON_GET_REQ)
	static public void NC_PRISON_GET_REQ(ClientWorld client, NC_PRISON_GET_REQ packet) {
		client.writePacket(new NC_PRISON_GET_ACK());
	}
}