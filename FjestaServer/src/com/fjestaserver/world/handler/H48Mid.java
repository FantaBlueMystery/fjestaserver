/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.world.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.mid.NC_MID_INSTANCE_LIST_ACK;
import com.fjestacore.packet.mid.NC_MID_INSTANCE_LIST_REQ;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.world.ClientWorld;

/**
 * H48Mid
 * @author FantaBlueMystery
 */
public class H48Mid {

	/**
	 * NC_MENU_SERVERMENU_ACK
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.WORLD, cmd = NcClient.NC_MID_INSTANCE_LIST_REQ)
	static public void NC_MID_INSTANCE_LIST_REQ(ClientWorld client, NC_MID_INSTANCE_LIST_REQ packet) {
		client.writePacket(new NC_MID_INSTANCE_LIST_ACK());
	}
}