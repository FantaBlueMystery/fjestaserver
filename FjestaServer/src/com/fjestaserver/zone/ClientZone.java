/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone;

import com.fjestacore.common.ColorID;
import com.fjestacore.common.act.Notice;
import com.fjestacore.common.chat.SomeoneChat;
import com.fjestacore.packet.act.NC_ACT_NOTICE_CMD;
import com.fjestacore.packet.act.NC_ACT_SOMEONECHAT_CMD;
import com.fjestacore.socket.ServerBase;
import com.fjestaserver.core.net.Client;
import java.net.Socket;

/**
 * ClientZone
 * @author FantaBlueMystery
 */
public class ClientZone extends Client {

	/**
	 * ClientZone
	 * @param server
	 * @param clientSocket
	 */
	public ClientZone(ServerBase server, Socket clientSocket) {
		super(server, clientSocket);
	}

	/**
	 * _getClassName
	 * @return
	 */
	@Override
	protected String _getClassName() {
		return this.getClass().getName().replace("com.fjestaserver.zone.", "");
	}

	/**
	 * getServer
	 * @return
	 */
	@Override
	public ServerZone getServer() {
		return (ServerZone) this._server;
	}

	/**
	 * sendChat
	 * @param content
	 */
	public void sendChat(String content) {
		this.sendChat(content, null);
	}

	/**
	 * sendChat
	 * @param content
	 * @param color
	 */
	public void sendChat(String content, ColorID color) {
		int handle = this.getSession().getCharacter().getObjectHandle();
		SomeoneChat sc;

		if( color != null ) {
			sc = new SomeoneChat(handle, 2, content, color.getValue());
		}
		else {
			sc = new SomeoneChat(handle, 2, content);
		}

		this.writePacket(new NC_ACT_SOMEONECHAT_CMD(sc));
	}

	/**
	 * sendNotice
	 * @param content
	 */
	public void sendNotice(String content) {
		this.sendNotice(new Notice(content));
	}

	/**
	 * sendNotice
	 * @param notice
	 */
	public void sendNotice(Notice notice) {
		this.writePacket(new NC_ACT_NOTICE_CMD(notice));
	}
}