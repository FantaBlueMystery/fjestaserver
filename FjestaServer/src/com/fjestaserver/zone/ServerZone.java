/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone;

import com.fjestacore.common.chat.SomeoneChat;
import com.fjestacore.common.map.MapDefault;
import com.fjestacore.common.position.PositionXY;
import com.fjestacore.common.act.Notice;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.act.NC_ACT_SOMEONECHAT_CMD;
import com.fjestacore.packet.act.NC_ACT_NOTICE_CMD;
import com.fjestacore.socket.ClientBase;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.item.ItemDrop;
import com.fjestaserver.core.mob.sample.Elderine;
import com.fjestaserver.core.mob.sample.OXField;
import com.fjestaserver.core.mob.sample.GMClara;
import com.fjestaserver.core.net.Client;
import com.fjestaserver.core.net.Server;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.handler.HandlerDictionary;
import com.fjestaserver.core.map.MapManager;
import com.fjestaserver.core.map.IMap;
import com.fjestaserver.zone.config.ZoneConfig;
import com.fjestaserver.zone.handler.H04Char;
import com.fjestaserver.zone.handler.H06Map;
import com.fjestaserver.zone.handler.H08Act;
import com.fjestaserver.zone.handler.H09Bat;
import com.fjestaserver.zone.handler.H12Item;
import com.fjestaserver.zone.handler.H15Menu;
import java.net.Socket;

/**
 * ServerZone
 * @author FantaBlueMystery
 */
public class ServerZone extends Server {

	/**
	 * map
	 */
	protected IMap _map = null;

	/**
	 * ServerZone
	 * @param prop
	 */
	public ServerZone(ZoneConfig prop) {
		super(prop);

		this._serverType = ServerType.ZONE;

		// TODO use MapDefault.getMapInfo()
		this._map = MapManager.getInstance().createMap(MapDefault.getMapInfo());
		this._map.setOwnerObject(this);
		this._map.setServer(this);

		HandlerDictionary.getInstance().addHandler(H04Char.class);
		HandlerDictionary.getInstance().addHandler(H06Map.class);
		HandlerDictionary.getInstance().addHandler(H08Act.class);
		HandlerDictionary.getInstance().addHandler(H09Bat.class);
		HandlerDictionary.getInstance().addHandler(H12Item.class);
		HandlerDictionary.getInstance().addHandler(H15Menu.class);

		// TODO
		// add test mob
		this._map.addObject(new Elderine());
		this._map.addObject(new OXField());

		GMClara gm = new GMClara();

		this._map.addObject(gm);
	}

	/**
	 * _castSocket
	 * @param cs
	 * @return
	 */
	@Override
	protected ClientZone _castSocket(Socket cs) {
		return new ClientZone(this, cs);
	}

	/**
	 * getMap
	 * @return
	 */
	public IMap getMap() {
		return this._map;
	}

	/**
	 * writePacketInRange
	 * @param pac
	 * @param position
	 * @return
	 */
	public int writePacketInRange(Packet pac, PositionXY position) {
		return this.writePacketInRange(pac, position, null);
	}

	/**
	 * writePacketInRange
	 * @param pac
	 * @param position
	 * @param ignorClientSessionId
	 * @return
	 */
	public int writePacketInRange(Packet pac, PositionXY position, String ignorClientSessionId) {
		int count = 0;

		for( ClientBase tclient: this._clients ) {
			Client c = (Client) tclient;

			ISession tSession = c.getSession();

			if( tSession != null ) {
				if( ignorClientSessionId != null ) {
					if( tSession.getId().compareTo(ignorClientSessionId) == 0 ) {
						continue;
					}
				}

				ICharacterServer tchar = tSession.getCharacter();

				if( tchar != null ) {
					if( tchar.getPosition().inRange(position) ) {
						c.writePacket(pac);

						count++;
					}
				}
			}
		}

		return count;
	}

	/**
	 * sendChat
	 * @param chat
	 * @param byPosition
	 */
	public void sendChat(SomeoneChat chat, PositionXY byPosition) {
		this.writePacketInRange(new NC_ACT_SOMEONECHAT_CMD(chat), byPosition);
	}

	/**
	 * dropItem
	 * @param item
	 */
	public void dropItem(ItemDrop item) {
		this.getMap().addObject(item);
	}

	/**
	 * sendNotice
	 * @param notice
	 */
	public void sendNotice(Notice notice) {
		this.writePacketBroadcast(new NC_ACT_NOTICE_CMD(notice));
	}
}