/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone.config;

import com.fjestacore.socket.config.SocketConfig;

/**
 * ZoneConfig
 * @author FantaBlueMystery
 */
public class ZoneConfig extends SocketConfig {

}