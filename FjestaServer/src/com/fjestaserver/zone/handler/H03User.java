/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.user.NC_USER_NORMALLOGOUT_CMD;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.zone.ClientZone;

/**
 * H03User
 * @author FantaBlueMystery
 */
public class H03User {

	/**
	 * NC_USER_NORMALLOGOUT_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_USER_NORMALLOGOUT_CMD)
	static public void NC_USER_NORMALLOGOUT_CMD(ClientZone client, NC_USER_NORMALLOGOUT_CMD packet) {
		
	}
}