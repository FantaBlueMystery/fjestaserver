/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.chars.NC_CHAR_LOGOUTREADY_CMD;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.handle.HandleManager;
import com.fjestaserver.core.handle.IHandleManager;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.zone.ClientZone;
import com.fjestaserver.zone.ServerZone;

/**
 * H04Char
 * @author FantaBlueMystery
 */
public class H04Char {

	/**
	 * NC_CHAR_LOGOUTREADY_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_CHAR_LOGOUTREADY_CMD)
	static public void NC_CHAR_LOGOUTREADY_CMD(ClientZone client, NC_CHAR_LOGOUTREADY_CMD packet) {
		ISession session = client.getSession();

		if( session != null ) {
			ICharacterServer cs = session.getCharacter();

			if( cs != null ) {
				ServerZone sz = client.getServer();
				sz.getMap().removeObject(cs.getObjectHandle());

				IHandleManager hm = HandleManager.getInstance();
				hm.resetObjectHandle(cs.getObjectHandle());

				session.setCharacter(null);
				return;
			}
		}

		client.getServer().removeClient(client);
	}
}
