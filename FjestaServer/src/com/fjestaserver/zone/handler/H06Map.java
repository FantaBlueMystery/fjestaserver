/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone.handler;

import com.fjestacore.common.login.MapLogin;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.map.NC_MAP_LOGINCOMPLETE_CMD;
import com.fjestacore.packet.map.NC_MAP_LOGIN_REQ;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.handle.HandleManager;
import com.fjestaserver.core.handle.IHandleManager;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.core.session.ISessionManager;
import com.fjestaserver.core.session.SessionManager;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.zone.ClientZone;
import com.fjestaserver.zone.ServerZone;

/**
 * H06Map
 * @author FantaBlueMystery
 */
public class H06Map {

	/**
	 * NC_MAP_LOGIN_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_MAP_LOGIN_REQ)
	static public void NC_MAP_LOGIN_REQ(ClientZone client, NC_MAP_LOGIN_REQ packet) {
		MapLogin ml = packet.getMapLogin();

		ISessionManager sm = SessionManager.getInstance();
		ISession session = sm.getSession(ml.getWorldHandle());

		if( session != null ) {
			ICharacterServer character = session.getCharacter();

			if( character != null ) {
				if( character.getCharname().compareTo(ml.getCharname()) == 0 ) {
					// set client
					character.setClientZone(client);

					// set session
					client.setSession(session);

					IHandleManager hm = HandleManager.getInstance();
					character.setObjectHandle(hm.getFreeObjectHandle());

					character.mapLogin();

					return;
				}
			}
		}

		client.getServer().removeClient(client);
	}

	/**
	 * NC_MAP_LOGINCOMPLETE_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_MAP_LOGINCOMPLETE_CMD)
	static public void NC_MAP_LOGINCOMPLETE_CMD(ClientZone client, NC_MAP_LOGINCOMPLETE_CMD packet)  {
		ISession session = client.getSession();

		if( session != null ) {
			ServerZone sz = client.getServer();

			ICharacterServer character = session.getCharacter();

			if( character instanceof IMapObject ) {
				sz.getMap().addObject((IMapObject) character);
			}

			character.mapLoginComplete();
		}
	}
}