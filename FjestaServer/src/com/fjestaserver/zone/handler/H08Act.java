/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone.handler;

import com.fjestacore.common.ColorID;
import com.fjestacore.common.chat.SomeoneChat;
import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.act.NC_ACT_CHAT_REQ;
import com.fjestacore.packet.act.NC_ACT_JUMP_CMD;
import com.fjestacore.packet.act.NC_ACT_STOP_REQ;
import com.fjestacore.packet.act.NC_ACT_MOVERUN_CMD;
import com.fjestacore.packet.act.NC_ACT_MOVEWALK_CMD;
import com.fjestacore.packet.act.NC_ACT_NPCCLICK_CMD;
import com.fjestacore.packet.act.NC_ACT_EMOTICON_CMD;
import com.fjestacore.packet.act.NC_ACT_EMOTICONSTOP_CMD;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.map.MapObjectList;
import com.fjestaserver.core.mob.event.IMobEventNPCClick;
import com.fjestaserver.core.session.ISession;
import com.fjestaserver.core.characters.ICharacterServer;
import com.fjestaserver.core.command.CommandManager;
import com.fjestaserver.core.command.CommandNotFoundException;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.zone.ClientZone;
import com.fjestaserver.zone.ServerZone;

/**
 * H08Act
 * @author FantaBlueMystery
 */
public class H08Act {

	/**
	 * NC_ACT_CHAT_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ACT_CHAT_REQ)
	static public void NC_ACT_CHAT_REQ(ClientZone client, NC_ACT_CHAT_REQ packet) {
		CommandManager cm = CommandManager.getInstance();

		try {
			String content = cm.process(packet.getChat().getContent(), client);

			if( content != null ) {
				packet.getChat().setContent(content);

				ICharacterServer cs = client.getSession().getCharacter();

				client.getServer().sendChat(new SomeoneChat(
					cs.getObjectHandle(),
					2,
					packet.getChat()), cs.getPosition());
			}
		}
		catch( CommandNotFoundException cnfe ) {
			client.sendChat("Command not found!", ColorID.SYSTEM_WARNING);
		}
	}

	/**
	 * NC_ACT_STOP_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ACT_STOP_REQ)
	static public void NC_ACT_STOP_REQ(ClientZone client, NC_ACT_STOP_REQ packet) {
		ISession tsession = client.getSession();

		if( tsession != null ) {
			ICharacterServer cs = tsession.getCharacter();
			cs.moveStop(packet.getPosition());
		}
	}

	/**
	 * NC_ACT_JUMP_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ACT_JUMP_CMD)
	static public void NC_ACT_JUMP_CMD(ClientZone client, NC_ACT_JUMP_CMD packet) {
		ISession tsession = client.getSession();

		if( tsession != null ) {
			tsession.getCharacter().useJump(true);
		}
	}

	/**
	 * NC_ACT_MOVERUN_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ACT_MOVERUN_CMD)
	static public void NC_ACT_MOVERUN_CMD(ClientZone client, NC_ACT_MOVERUN_CMD packet) {
		ISession tsession = client.getSession();

		if( tsession != null ) {
			ICharacterServer cs = tsession.getCharacter();
			cs.moveRun(packet.getMove());
		}
	}

	/**
	 * NC_ACT_MOVEWALK_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ACT_MOVEWALK_CMD)
	static public void NC_ACT_MOVEWALK_CMD(ClientZone client, NC_ACT_MOVEWALK_CMD packet) {
		ISession tsession = client.getSession();

		if( tsession != null ) {
			ICharacterServer cs = tsession.getCharacter();
			cs.moveWalk(packet.getMove());
		}
	}

	/**
	 * NC_ACT_NPCCLICK_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ACT_NPCCLICK_CMD)
	static public void NC_ACT_NPCCLICK_CMD(ClientZone client, NC_ACT_NPCCLICK_CMD packet) {
		ServerZone sz = client.getServer();

		int handle = packet.getNpcClick().getObjectHandle();
		MapObjectList mol = sz.getMap().getObjectList();

		IMapObject tmob = mol.getMob(handle);

		if( tmob instanceof IMobEventNPCClick ) {
			((IMobEventNPCClick) tmob).onNPCClick(client);
		}
	}

	/**
	 * NC_ACT_EMOTICON_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ACT_EMOTICON_CMD)
	static public void NC_ACT_EMOTICON_CMD(ClientZone client, NC_ACT_EMOTICON_CMD packet) {
		ISession tsession = client.getSession();

		if( tsession != null ) {
			ICharacterServer cs = client.getSession().getCharacter();
			cs.useEmote(packet.getEmoticon().getId());
		}
	}

	/**
	 * NC_ACT_EMOTICONSTOP_CMD
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ACT_EMOTICONSTOP_CMD)
	static public void NC_ACT_EMOTICONSTOP_CMD(ClientZone client, NC_ACT_EMOTICONSTOP_CMD packet) {
		ISession tsession = client.getSession();

		if( tsession != null ) {
			ICharacterServer cs = client.getSession().getCharacter();
			cs.stopEmote();
		}
	}
}