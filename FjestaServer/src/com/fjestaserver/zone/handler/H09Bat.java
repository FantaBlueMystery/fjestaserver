/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.bat.NC_BAT_TARGETINFO_CMD;
import com.fjestacore.packet.bat.NC_BAT_TARGETTING_REQ;
import com.fjestacore.packet.bat.NC_BAT_UNTARGET_REQ;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.map.MapObjectList;
import com.fjestaserver.core.mob.event.IMobEventNPCClick;
import com.fjestaserver.core.target.ITarget;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.zone.ClientZone;
import com.fjestaserver.zone.ServerZone;

/**
 * H09Bat
 * @author FantaBlueMystery
 */
public class H09Bat {

	/**
	 * NC_BAT_TARGETTING_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_BAT_TARGETTING_REQ)
	static public void NC_BAT_TARGETTING_REQ(ClientZone client, NC_BAT_TARGETTING_REQ packet) {
		ServerZone sz = client.getServer();

		int objectid = packet.getTargetting().getObjectId();

		MapObjectList mol = sz.getMap().getObjectList();
		IMapObject object = mol.getByObjectId(objectid);

		if( object != null ) {
			client.getSession().setSelectedTarget(object);

			if( object instanceof IMobEventNPCClick ) {
				((IMobEventNPCClick) object).onNPCClick(client);
			}

			if( object instanceof ITarget ) {
				client.writePacket(new NC_BAT_TARGETINFO_CMD(
					((ITarget)object).getTargetInfo()
					));
			}
		}
	}

	/**
	 * NC_BAT_UNTARGET_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_BAT_UNTARGET_REQ)
	static public void NC_BAT_UNTARGET_REQ(ClientZone client, NC_BAT_UNTARGET_REQ packet) {
		client.getSession().setSelectedTarget(null);
	}
}
