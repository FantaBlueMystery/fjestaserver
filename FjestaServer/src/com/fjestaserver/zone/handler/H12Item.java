/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.item.NC_ITEM_CHARGEDINVENOPEN_ACK;
import com.fjestacore.packet.item.NC_ITEM_CHARGEDINVENOPEN_REQ;
import com.fjestacore.packet.item.NC_ITEM_PICK_REQ;
import com.fjestacore.packet.item.NC_ITEM_USE_REQ;
import com.fjestaserver.core.item.ItemDrop;
import com.fjestaserver.core.map.IMapObject;
import com.fjestaserver.core.map.MapObjectList;
import com.fjestaserver.core.map.MapObjectType;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.zone.ClientZone;
import com.fjestaserver.zone.ServerZone;

/**
 * H12Item
 * @author FantaBlueMystery
 */
public class H12Item {

	/**
	 * NC_ITEM_CHARGEDINVENOPEN_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ITEM_CHARGEDINVENOPEN_REQ)
	static public void NC_ITEM_CHARGEDINVENOPEN_REQ(ClientZone client, NC_ITEM_CHARGEDINVENOPEN_REQ packet) {
		// TODO
		client.writePacket(new NC_ITEM_CHARGEDINVENOPEN_ACK());
	}

	/**
	 * NC_ITEM_USE_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ITEM_USE_REQ)
	static public void NC_ITEM_USE_REQ(ClientZone client, NC_ITEM_USE_REQ packet) {
		
	}

	/**
	 * NC_ITEM_PICK_REQ
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_ITEM_PICK_REQ)
	static public void NC_ITEM_PICK_REQ(ClientZone client, NC_ITEM_PICK_REQ packet) {
		ServerZone sz = client.getServer();

		int handle = packet.getItem().getObjectHandle();
		MapObjectList mol = sz.getMap().getObjectList();

		IMapObject object = mol.getMob(handle);

		if (object instanceof ItemDrop) {
			ItemDrop item = (ItemDrop)object;
			// item.getItemId() // Actually give item to player;

			client.sendChat("Eigentlich solltest du jetzt item " + item.getItemId() + " bekommen.");

			mol.removeByObjectId(handle, MapObjectType.ITEM);
		}
	}
}
