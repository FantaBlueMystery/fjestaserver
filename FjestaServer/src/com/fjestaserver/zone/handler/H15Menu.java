/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestaserver.zone.handler;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.menu.NC_MENU_SERVERMENU_ACK;
import com.fjestaserver.core.menu.IMenu;
import com.fjestaserver.handler.HandlerDefine;
import com.fjestaserver.zone.ClientZone;

/**
 * H15Menu
 * @author FantaBlueMystery
 */
public class H15Menu {

	/**
	 * NC_MENU_SERVERMENU_ACK
	 * @param client
	 * @param packet
	 */
	@HandlerDefine(type = ServerType.ZONE, cmd = NcClient.NC_MENU_SERVERMENU_ACK)
	static public void NC_MENU_SERVERMENU_ACK(ClientZone client, NC_MENU_SERVERMENU_ACK packet) {
		IMenu sm = client.getSession().getUsedMenu();

		if( sm != null ) {
			sm.onMenuAction(packet.getServerMenu().getReply(), client);
		}
	}
}